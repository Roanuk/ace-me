package com.roanuk.aceme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

public class CourseTimeAdapter extends ArrayAdapter<CourseMeeting> implements Serializable {
    public CourseTimeAdapter(Context context, ArrayList<CourseMeeting> assignment) {
        super(context, 0, assignment);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        CourseMeeting meeting = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.short_assignments_list_item, parent, false);
        }
        // Lookup view for data population
        TextView tvCourse = (TextView) convertView.findViewById(R.id.tvCourse);
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        // Populate the data into the template view using the data object
        Day day = meeting.getDay();
        if(day.equals(Day.MONDAY)){
            tvCourse.setText("M");
        } else if(day.equals(Day.TUESDAY)){
            tvCourse.setText("T");
        } else if(day.equals(Day.WEDNESDAY)){
            tvCourse.setText("W");
        } else if(day.equals(Day.THURSDAY)){
            tvCourse.setText("Th");
        } else if(day.equals(Day.FRIDAY)){
            tvCourse.setText("F");
        } else if(day.equals(Day.SATURDAY)){
            tvCourse.setText("Sat");
        } else if(day.equals(Day.SUNDAY)){
            tvCourse.setText("Sun");
        }

        String am = "AM";
        int sh = meeting.getStartHour();
        int sm = meeting.getStartMinute();
        int eh = meeting.getEndHour();
        int em = meeting.getEndMinute();

        if(sh > 12){
            sh = sh - 12;
            am = "PM";
        }
        if(eh > 12){
            eh = eh - 12;
            am = "PM";
        }

        String one = "" + sh;
        String two = "" + sm;
        String three = "" + eh;
        String four = "" + em;

        if(two.length() == 1){
            two = "0" + sm;
        }
        if(four.length() == 1){
            four = "0" + em;
        }

        tvName.setText("" + one + ":" + two + "-" + three + ":" + four + " " + am);
        // Return the completed view to render on screen
        return convertView;
    }
}