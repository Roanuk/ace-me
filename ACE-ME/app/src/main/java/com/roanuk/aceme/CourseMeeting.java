package com.roanuk.aceme;

import java.io.Serializable;
import java.util.Calendar;

/**
 * class used to represent the meeting of a course such as a lecture or lab
 */
public class CourseMeeting implements Serializable, Comparable<CourseMeeting> {
    Day day;
    int startHour;
    int startMinute;
    int endHour;
    int endMinute;
    String room;

    public CourseMeeting(){
        day = null;
        startHour = 0;
        startMinute = 0;
        endHour = 0;
        endMinute = 0;
        room = "";
    }

    public CourseMeeting(Day day, int sh, int sm, int eh, int em, String room){
        this.day = day;
        startHour = sh;
        startMinute = sm;
        endHour = eh;
        endMinute = em;
        this.room = room;
    }

    /* getter and setter methods */
    public Day getDay() {
        return day;
    }

    public int getCalendarDay()
    {
        switch (day)
        {
            case SUNDAY:
                return Calendar.SUNDAY;

            case MONDAY:
                return Calendar.MONDAY;

            case TUESDAY:
                return Calendar.TUESDAY;

            case WEDNESDAY:
                return Calendar.WEDNESDAY;

            case THURSDAY:
                return Calendar.THURSDAY;

            case FRIDAY:
                return Calendar.FRIDAY;

            case SATURDAY:
                return Calendar.SATURDAY;

            default:
                return 0;
        }
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(int startMinute) {
        this.startMinute = startMinute;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public int getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(int endMinute) {
        this.endMinute = endMinute;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int compareTo(CourseMeeting another) {
        if(another.getCalendarDay() > this.getCalendarDay()) { return -1; }
        if(another.getCalendarDay() < this.getCalendarDay()) { return 1; }
        else {
            if(another.getStartMinute() > this.getStartHour()){
                return -1;
            } else if(another.getStartMinute() < this.getStartHour()){
                return 1;
            }
            else{
                if(another.getStartMinute() > this.getStartMinute()){
                    return -1;
                }
                else{
                    return 1;
                }
            }
        }
    }
}
