package com.roanuk.aceme;

import java.io.Serializable;

/**
 * Priority values
 */
public enum Priority implements Serializable
{
    HIGHEST("Highest", 5),
    HIGH("High", 4),
    MEDIUM("Medium", 3),
    LOW("Low", 2),
    LOWEST("Lowest", 1),
    NONE("None", 0);

    private String name;
    private int val;

    private Priority(String name, int x){
        this.name = name;
        this.val = x;
    }

    public int getVal(){
        return this.val;
    }

    @Override
    public String toString() { return this.name; }
}

