package com.roanuk.aceme;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
// import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
// import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class EditCourseActivity extends ActionBarActivity {

    CourseManager courseManager;
    private CourseTimes ct = new CourseTimes();
    private Course currentCourse;
    public Calendar cal;
    public int beginday;
    public int beginmonth;
    public int beginyear;
    public int endday;
    public int endmonth;
    public int endyear;
    public int starthour;
    public int startminute;
    public int endhour;
    public int endminute;
    public static final SimpleDateFormat DATE_FORMAT =
            new SimpleDateFormat("MM/dd/yyyy", Locale.US);
    public static final SimpleDateFormat TIME_FORMAT =
            new SimpleDateFormat("h:mm a", Locale.US);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_course);

        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());

        currentCourse = courseManager.getCurrentCourse();

        rebuild();
    }

    public void rebuild(){
        ((EditText) findViewById(R.id.title)).setText(currentCourse.getCourseTitle());
        ((EditText) findViewById(R.id.nt)).setText(currentCourse.getNotes());
        ((EditText) findViewById(R.id.code)).setText(currentCourse.getCourseCode());
        ((EditText) findViewById(R.id.professor2)).setText(currentCourse.getProfessor());
        ((EditText) findViewById(R.id.syllubus2)).setText(currentCourse.getSyllabusPath());

        rebuildList();
    }

    public void rebuildList(){
        ct = currentCourse.getCourseTimes();
        final ArrayList<CourseMeeting> meetings = ct.getMeetings();

        CourseTimeAdapter adapter = new CourseTimeAdapter(this, meetings);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showRemovePopUp(meetings.get(position));
            }
        });

        cal = Calendar.getInstance(TimeZone.getDefault());
        cal.setTime(currentCourse.getStartDate());
        beginday = cal.get(Calendar.DAY_OF_MONTH);
        beginmonth = cal.get(Calendar.MONTH);
        beginyear = cal.get(Calendar.YEAR);
        cal.setTime(currentCourse.getEndDate());
        endday = cal.get(Calendar.DAY_OF_MONTH);
        endmonth = cal.get(Calendar.MONTH);
        endyear = cal.get(Calendar.YEAR);
        ((TextView)findViewById(R.id.textView25)).setText(AddNewCourseActivity.DATE_FORMAT.format(currentCourse.getStartDate()));
        ((TextView)findViewById(R.id.textView26)).setText(AddNewCourseActivity.DATE_FORMAT.format(currentCourse.getEndDate()));

        starthour = cal.get(Calendar.HOUR_OF_DAY);
        startminute  = cal.get(Calendar.MINUTE);
        endhour = cal.get(Calendar.HOUR_OF_DAY);
        endminute  = cal.get(Calendar.MINUTE);
        ((TextView)findViewById(R.id.textView252)).setText(AddNewCourseActivity.TIME_FORMAT.format(cal.getTime()));
        ((TextView)findViewById(R.id.textView262)).setText(AddNewCourseActivity.TIME_FORMAT.format(cal.getTime()));

        GradeSystem gradeSystem = currentCourse.getGradeSystem();
        ((EditText) findViewById(R.id.hw)).setText(Double.toString(gradeSystem.getHomeworkWeight()));
        ((EditText) findViewById(R.id.quiz)).setText(Double.toString(gradeSystem.getQuizWeight()));
        ((EditText) findViewById(R.id.project)).setText(Double.toString(gradeSystem.getProjectWeight()));
        ((EditText) findViewById(R.id.exam)).setText(Double.toString(gradeSystem.getExamWeight()));
        ((EditText) findViewById(R.id.finals2)).setText(Double.toString(gradeSystem.getFinalWeight()));   // */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_add_new_course, menu);
        //Toast.makeText(getApplicationContext(), "Sorry. You can't add during an edit.", Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_new_event) {
            courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
            if(courseManager.getCourses().isEmpty()){
                Toast.makeText(getApplicationContext(), "You need a course to do that!", Toast.LENGTH_LONG).show();
            }
            else {
                Intent intent = new Intent(this, AddNewEventActivity.class);
                startActivity(intent);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void addAnotherCourseTimeEdit(View view){
        if(((CheckBox) findViewById(R.id.checkBox5)).isChecked()){
            ct.addTime(Day.SUNDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox6)).isChecked()){
            ct.addTime(Day.MONDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox7)).isChecked()){
            ct.addTime(Day.TUESDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox8)).isChecked()){
            ct.addTime(Day.WEDNESDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox9)).isChecked()){
            ct.addTime(Day.THURSDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox10)).isChecked()){
            ct.addTime(Day.FRIDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox11)).isChecked()){
            ct.addTime(Day.SATURDAY, starthour, startminute, endhour, endminute, null);
        }

        Toast.makeText(getApplicationContext(), "Class time saved!", Toast.LENGTH_LONG).show();

        ((CheckBox) findViewById(R.id.checkBox5)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkBox6)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkBox7)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkBox8)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkBox9)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkBox10)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkBox11)).setChecked(false);

        rebuildList();
    }

    public void saveCourseEdit(View view){
        String title = ((EditText) findViewById(R.id.title)).getText().toString();
        if(title.equals("") || title == null){
            title = "Untitled Course";
        }

        String notes = ((EditText) findViewById(R.id.nt)).getText().toString();
        String courseCode = ((EditText) findViewById(R.id.code)).getText().toString();
        String professor = ((EditText) findViewById(R.id.professor2)).getText().toString();
        String syllubus = ((EditText) findViewById(R.id.syllubus2)).getText().toString();


        if(((CheckBox) findViewById(R.id.checkBox5)).isChecked()){
            ct.addTime(Day.SUNDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox6)).isChecked()){
            ct.addTime(Day.MONDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox7)).isChecked()){
            ct.addTime(Day.TUESDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox8)).isChecked()){
            ct.addTime(Day.WEDNESDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox9)).isChecked()){
            ct.addTime(Day.THURSDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox10)).isChecked()){
            ct.addTime(Day.FRIDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkBox11)).isChecked()){
            ct.addTime(Day.SATURDAY, starthour, startminute, endhour, endminute, null);

        }

        Calendar begin = Calendar.getInstance();
        begin.set(Calendar.MONTH, beginmonth);
        begin.set(Calendar.DAY_OF_MONTH, beginday);
        begin.set(Calendar.YEAR, beginyear);

        Calendar end = Calendar.getInstance();
        end.set(Calendar.MONTH, endmonth);
        end.set(Calendar.DAY_OF_MONTH, endday);
        end.set(Calendar.YEAR, endyear);



        double homework = Double.parseDouble(((EditText) findViewById(R.id.hw)).getText().toString());
        double quizzes = Double.parseDouble(((EditText) findViewById(R.id.quiz)).getText().toString());
        double projects = Double.parseDouble(((EditText) findViewById(R.id.project)).getText().toString());
        double exams = Double.parseDouble(((EditText) findViewById(R.id.exam)).getText().toString());
        double finals = Double.parseDouble(((EditText) findViewById(R.id.finals2)).getText().toString());
        GradeSystem gradeSystem = new GradeSystem();
        gradeSystem.setHomeworkWeight(homework);
        gradeSystem.setQuizWeight(quizzes);
        gradeSystem.setProjectWeight(projects);
        gradeSystem.setExamWeight(exams);
        gradeSystem.setFinalWeight(finals);

        CourseAssignments assignments = currentCourse.getAssignments();

        courseManager.removeCourse(currentCourse);

        courseManager.addNewCourse(title, courseCode, begin.getTime(), end.getTime(), ct, notes, professor, null, syllubus, gradeSystem);

        Alarms.RemindCourse(this,courseManager.getCourse(courseManager.getCourses().size() - 1));
        courseManager.getCourse(courseManager.getCourses().size() - 1).setAssignments(assignments);

        InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);

        Toast.makeText(getApplicationContext(), "Class saved!", Toast.LENGTH_LONG).show();
        Intent returnIntent = new Intent();
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    private void showRemovePopUp(CourseMeeting meeting) {
        final CourseMeeting meeting2 = meeting;
        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Alert");
        helpBuilder.setMessage("Delete selected class time?");
        helpBuilder.setPositiveButton("Delete",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                       ct.removeTime(meeting2);
                       rebuildList();
                    }
                });
        helpBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                    }
                });
        // Remember, create doesn't show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }

    public void removeCourseEdit(View view){
        AlertDialog.Builder deleteAlert = new AlertDialog.Builder(this);
        deleteAlert.setTitle(getString(R.string.title_dialog_delete_course));
        deleteAlert.setMessage(R.string.description_dialog_delete_course);

        deleteAlert.setPositiveButton(getString(R.string.dialog_delete_affirmative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        courseManager.removeCourse(currentCourse);
                        InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);
                        Toast.makeText(getApplicationContext(), "Class deleted!", Toast.LENGTH_LONG).show();
                        Intent returnIntent = new Intent();
                        setResult(RESULT_CANCELED, returnIntent);
                        finish();
                    }
                });
        deleteAlert.setNegativeButton(getString(R.string.dialog_negative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        deleteAlert.show();
    }

    public void setDate1Edit(View view) {
        cal = Calendar.getInstance(TimeZone.getDefault());
        DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                beginday = d;
                beginmonth = m;
                beginyear = y;
                cal.set(Calendar.DAY_OF_MONTH, beginday);
                cal.set(Calendar.MONTH, beginmonth);
                cal.set(Calendar.YEAR, beginyear);
                ((TextView)findViewById(R.id.textView25)).setText(EditCourseActivity.DATE_FORMAT.format(cal.getTime()));
            }
        }, this.beginyear, this.beginmonth, this.beginday);
        datePicker.setCancelable(false);
        datePicker.setTitle("Set Beginning Date...");
        datePicker.show();
    }

    public void setDate2Edit(View view) {
        cal = Calendar.getInstance(TimeZone.getDefault());
        DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                endday = d;
                endmonth = m;
                endyear = y;
                cal.set(Calendar.DAY_OF_MONTH, endday);
                cal.set(Calendar.MONTH, endmonth);
                cal.set(Calendar.YEAR, endyear);
                ((TextView)findViewById(R.id.textView26)).setText(EditCourseActivity.DATE_FORMAT.format(cal.getTime()));
            }
        }, this.endyear, this.endmonth, this.endday);
        datePicker.setCancelable(false);
        datePicker.setTitle("Set End Date...");
        datePicker.show();
    }

    public void setTime1Edit(View view) {
        cal = Calendar.getInstance(TimeZone.getDefault());
        TimePickerDialog timePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int h, int m) {
                starthour = h;
                startminute = m;
                cal.set(Calendar.HOUR_OF_DAY, starthour);
                cal.set(Calendar.MINUTE, startminute);
                ((TextView) findViewById(R.id.textView252)).setText(EditCourseActivity.TIME_FORMAT.format(cal.getTime()));
            }
        }, this.starthour, this.startminute, true);
        timePicker.setCancelable(false);
        timePicker.setTitle("Set Start Time...");
        timePicker.show();
    }

    public void setTime2Edit(View view) {
        cal = Calendar.getInstance(TimeZone.getDefault());
        TimePickerDialog timePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int h, int m) {
                endhour = h;
                endminute = m;
                cal.set(Calendar.HOUR_OF_DAY, endhour);
                cal.set(Calendar.MINUTE, endminute);
                ((TextView) findViewById(R.id.textView262)).setText(EditCourseActivity.TIME_FORMAT.format(cal.getTime()));
            }
        }, this.endhour, this.endminute, true);
        timePicker.setCancelable(false);
        timePicker.setTitle("Set End Time...");
        timePicker.show();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder quitAlert = new AlertDialog.Builder(this);
        quitAlert.setTitle(getString(R.string.title_dialog_quit_without_saving));
        quitAlert.setMessage(R.string.description_dialog_quit_without_saving);

        quitAlert.setPositiveButton(getString(R.string.dialog_quit_affirmative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        quitAlert.setNegativeButton(getString(R.string.dialog_negative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { }
                });

        quitAlert.show();
    }

}
