package com.roanuk.aceme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class GradeViewActivity extends Activity {

    CourseManager courseManager;
    private static final int REFRESH_LIST = 0;
    private static ArrayList<Assignment> assignments;
    private CourseAdapter adapter;
    private GradeListAdapter adapter2;
    private ArrayList<Double> homeworkGrades;
    private ArrayList<Double> quizGrades;
    private ArrayList<Double> projectGrades;
    private ArrayList<Double> examGrades;
    private ArrayList<Double> finalGrades;
    private ArrayList<Double> homeworkGradesWeight;
    private ArrayList<Double> quizGradesWeight;
    private ArrayList<Double> projectGradesWeight;
    private ArrayList<Double> examGradesWeight;
    private ArrayList<Double> finalGradesWeight;
    private GradeSystem gradeSystem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        homeworkGrades = new ArrayList<>();
        homeworkGradesWeight = new ArrayList<>();
        quizGrades = new ArrayList<>();
        quizGradesWeight = new ArrayList<>();
        projectGrades = new ArrayList<>();
        projectGradesWeight = new ArrayList<>();
        examGrades = new ArrayList<>();
        examGradesWeight = new ArrayList<>();
        finalGrades = new ArrayList<>();
        finalGradesWeight = new ArrayList<>();

        rebuildGradeList();
    }

    /* refreshes grade list */
    public void rebuildGradeList() {
        // before you display a layout, determine if the user has added any courses yet
        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        if (courseManager.getCourses().isEmpty()) {
            setContentView(R.layout.no_grades);
        } else {
            setContentView(R.layout.activity_grade_view);

            ArrayList<Course> courses = courseManager.getCourses();
            adapter = new CourseAdapter(this, courses);

            Spinner spinner = ((Spinner) findViewById(R.id.spinner));
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new Listener());

            Course course = (Course) spinner.getItemAtPosition(spinner.getSelectedItemPosition());
            PriorityQueue<Assignment> courseAssignments = new PriorityQueue<>(course.getAssignments().getCourseAssignments());
            assignments = new ArrayList<>();
            if(courseAssignments.isEmpty()) Toast.makeText(this, "No Grades", Toast.LENGTH_SHORT);
            while(!courseAssignments.isEmpty()){
                assignments.add(courseAssignments.poll());
            }

            adapter2 = new GradeListAdapter(this, assignments);
            ListView listView = ((ListView) findViewById(R.id.gradeList));
            listView.setAdapter(adapter2);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(view.getContext(), EditAssignmentActivity.class);
                    courseManager.setCurrentAssignment(GradeViewActivity.getListOfAssignments().get(position));
                    InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);
                    startActivityForResult(intent, REFRESH_LIST);
                }
            });
            setListViewHeightBasedOnChildren(listView);

            for(int i = 0; i < assignments.size(); i++){
                if(assignments.get(i).instanceOf().equals(AssignmentType.HOMEWORK)){
                    homeworkGrades.add(assignments.get(i).getGrade());
                    homeworkGradesWeight.add(assignments.get(i).getGradeWeight());
                } else if(assignments.get(i).instanceOf().equals(AssignmentType.QUIZ)){
                    quizGrades.add(assignments.get(i).getGrade());
                    quizGradesWeight.add(assignments.get(i).getGradeWeight());
                } else if(assignments.get(i).instanceOf().equals(AssignmentType.PROJECT)){
                    projectGrades.add(assignments.get(i).getGrade());
                    projectGradesWeight.add(assignments.get(i).getGradeWeight());
                } else if(assignments.get(i).instanceOf().equals(AssignmentType.EXAM)){
                    examGrades.add(assignments.get(i).getGrade());
                    examGradesWeight.add(assignments.get(i).getGradeWeight());
                } else if(assignments.get(i).instanceOf().equals(AssignmentType.FINAL)){
                    finalGrades.add(assignments.get(i).getGrade());
                    finalGradesWeight.add(assignments.get(i).getGradeWeight());
                }
            }

            gradeSystem = course.getGradeSystem();
            buildGrades();
        }

    }

    public void buildGrades(){
        double total1 = 0;
        double total2 = 0;
        double total3 = 0;
        double total4 = 0;
        double total5 = 0;
        CheckBox box = (CheckBox) findViewById(R.id.checkBox12);
        if(box.isChecked()){
            double zero1 = 0;
            double zero2 = 0;
            double zero3 = 0;
            double zero4 = 0;
            double zero5 = 0;
            for(int i = 0; i < homeworkGrades.size(); i++){
                total1 += (homeworkGrades.get(i) * homeworkGradesWeight.get(i));
                zero1 += homeworkGradesWeight.get(i);
            }
            for(int i = 0; i < quizGrades.size(); i++){
                total2 += (quizGrades.get(i) * quizGradesWeight.get(i));
                zero2 += quizGradesWeight.get(i);
            }
            for(int i = 0; i < projectGrades.size(); i++){
                total3 += (projectGrades.get(i) * projectGradesWeight.get(i));
                zero3 += projectGradesWeight.get(i);
            }
            for(int i = 0; i < examGrades.size(); i++){
                total4 += (examGrades.get(i) * examGradesWeight.get(i));
                zero4 += examGradesWeight.get(i);
            }
            for(int i = 0; i < finalGrades.size(); i++){
                total5 += (finalGrades.get(i) * finalGradesWeight.get(i));
                zero5 += finalGradesWeight.get(i);
            }
            if(!homeworkGrades.isEmpty()) {
                total1 = total1 / zero1;
            }
            if(!quizGrades.isEmpty()) {
                total2 = total2 / zero2;
            }
            if(!projectGrades.isEmpty()) {
                total3 = total3 / zero3;
            }
            if(!examGrades.isEmpty()) {
                total4 = total4 / zero4;
            }
            if(!finalGrades.isEmpty()) {
                total5 = total5 / zero5;
            }

            int index1 = (Double.toString(total1)).indexOf(".") + 2;
            int index2 = (Double.toString(total2)).indexOf(".") + 2;
            int index3 = (Double.toString(total3)).indexOf(".") + 2;
            int index4 = (Double.toString(total4)).indexOf(".") + 2;
            int index5 = (Double.toString(total5)).indexOf(".") + 2;
            ((TextView) findViewById(R.id.textView38)).setText("" + (Double.toString(total1)).substring(0, index1) + "%");
            ((TextView) findViewById(R.id.textView40)).setText("" + (Double.toString(total2)).substring(0, index2) + "%");
            ((TextView) findViewById(R.id.textView42)).setText("" + (Double.toString(total3)).substring(0, index3) + "%");
            ((TextView) findViewById(R.id.textView46)).setText("" + (Double.toString(total4)).substring(0, index4) + "%");
            ((TextView) findViewById(R.id.textView44)).setText("" + (Double.toString(total5)).substring(0, index5) + "%");

            double weight1 = gradeSystem.getHomeworkWeight();
            double weight2 = gradeSystem.getQuizWeight();
            double weight3 = gradeSystem.getProjectWeight();
            double weight4 = gradeSystem.getExamWeight();
            double weight5 = gradeSystem.getFinalWeight();

            double average = (total1*(weight1/100))
                    + (total2*(weight2/100))
                    + (total3*(weight3/100))
                    + (total4*(weight4/100))
                    + (total5*(weight5/100));

            int index = (Double.toString(average)).indexOf(".") + 2;

            ((TextView) findViewById(R.id.textView50)).setText("" + (Double.toString(average)).substring(0, index) + "%");
        }
        else{
            double one = 0;
            double two = 0;
            double three = 0;
            double four = 0;
            double five = 0;
            double zero1 = 0;
            double zero2 = 0;
            double zero3 = 0;
            double zero4 = 0;
            double zero5 = 0;
            for(int i = 0; i < homeworkGrades.size(); i++){
                total1 += (homeworkGrades.get(i) * homeworkGradesWeight.get(i));
                one += homeworkGradesWeight.get(i);
                if(homeworkGrades.get(i) == 0){
                    zero1 += homeworkGradesWeight.get(i);
                }
            }
            for(int i = 0; i < quizGrades.size(); i++){
                total2 += (quizGrades.get(i) * quizGradesWeight.get(i));
                two += quizGradesWeight.get(i);
                if(quizGrades.get(i) == 0){
                    zero2 += quizGradesWeight.get(i);
                }
            }
            for(int i = 0; i < projectGrades.size(); i++){
                total3 += (projectGrades.get(i) * projectGradesWeight.get(i));
                three += projectGradesWeight.get(i);
                if(projectGrades.get(i) == 0){
                    zero3 += projectGradesWeight.get(i);
                }
            }
            for(int i = 0; i < examGrades.size(); i++){
                total4 += (examGrades.get(i) * examGradesWeight.get(i));
                four += examGradesWeight.get(i);
                if(examGrades.get(i) == 0){
                    zero4 += examGradesWeight.get(i);
                }
            }
            for(int i = 0; i < finalGrades.size(); i++){
                total5 += (finalGrades.get(i) * finalGradesWeight.get(i));
                five += finalGradesWeight.get(i);
                if(finalGrades.get(i) == 0){
                    zero5 += finalGradesWeight.get(i);
                }
            }
            if(!homeworkGrades.isEmpty() && one != zero1) {
                total1 = total1 / (one - zero1);
            }
            if(!quizGrades.isEmpty() && two != zero2) {
                total2 = total2 / (two - zero2);
            }
            if(!projectGrades.isEmpty() && three != zero3) {
                total3 = total3 / (three - zero3);
            }
            if(!examGrades.isEmpty() && four != zero4) {
                total4 = total4 / (four - zero4);
            }
            if(!finalGrades.isEmpty() && five != zero5) {
                total5 = total5 / (five - zero5);
            }

            int index1 = (Double.toString(total1)).indexOf(".") + 2;
            int index2 = (Double.toString(total2)).indexOf(".") + 2;
            int index3 = (Double.toString(total3)).indexOf(".") + 2;
            int index4 = (Double.toString(total4)).indexOf(".") + 2;
            int index5 = (Double.toString(total5)).indexOf(".") + 2;
            ((TextView) findViewById(R.id.textView38)).setText("" + (Double.toString(total1)).substring(0, index1) + "%");
            ((TextView) findViewById(R.id.textView40)).setText("" + (Double.toString(total2)).substring(0, index2) + "%");
            ((TextView) findViewById(R.id.textView42)).setText("" + (Double.toString(total3)).substring(0, index3) + "%");
            ((TextView) findViewById(R.id.textView46)).setText("" + (Double.toString(total4)).substring(0, index4) + "%");
            ((TextView) findViewById(R.id.textView44)).setText("" + (Double.toString(total5)).substring(0, index5) + "%");

            double weight1 = gradeSystem.getHomeworkWeight();
            double weight2 = gradeSystem.getQuizWeight();
            double weight3 = gradeSystem.getProjectWeight();
            double weight4 = gradeSystem.getExamWeight();
            double weight5 = gradeSystem.getFinalWeight();

            double average = (total1*(weight1/100))
                    + (total2*(weight2/100))
                    + (total3*(weight3/100))
                    + (total4*(weight4/100))
                    + (total5*(weight5/100));

            double notNeeded = 0;
            if(total1 != 0){
                notNeeded += weight1;
            }
            if(total2 != 0){
                notNeeded += weight2;
            }
            if(total3 != 0){
                notNeeded += weight3;
            }
            if(total4 != 0){
                notNeeded += weight4;
            }
            if(total5 != 0){
                notNeeded += weight5;
            }
            average = (average / notNeeded) * 100;

            int index = (Double.toString(average)).indexOf(".") + 2;

            ((TextView) findViewById(R.id.textView50)).setText("" + (Double.toString(average)).substring(0, index) + "%");
        }
    }

    private class Listener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position2, long id) {
            Spinner spinner = ((Spinner) findViewById(R.id.spinner));
            Course course = (Course) spinner.getItemAtPosition(position2);
            PriorityQueue<Assignment> a = new PriorityQueue<>(course.getAssignments().getCourseAssignments());
            assignments = new ArrayList<>();
            while(!a.isEmpty()){
                assignments.add(a.poll());
            }
            GradeListAdapter adapter2 = new GradeListAdapter(parent.getContext(), assignments);
            ListView listView = ((ListView) findViewById(R.id.gradeList));
            listView.setAdapter(adapter2);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(view.getContext(), EditAssignmentActivity.class);
                    courseManager.setCurrentAssignment(GradeViewActivity.getListOfAssignments().get(position));
                    InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);
                    startActivityForResult(intent, REFRESH_LIST);
                }
            });
            setListViewHeightBasedOnChildren(listView);
            homeworkGrades = new ArrayList<>();
            homeworkGradesWeight = new ArrayList<>();
            quizGrades = new ArrayList<>();
            quizGradesWeight = new ArrayList<>();
            projectGrades = new ArrayList<>();
            projectGradesWeight = new ArrayList<>();
            examGrades = new ArrayList<>();
            examGradesWeight = new ArrayList<>();
            finalGrades = new ArrayList<>();
            finalGradesWeight = new ArrayList<>();
            for(int i = 0; i < assignments.size(); i++){
                if(assignments.get(i).instanceOf().equals(AssignmentType.HOMEWORK)){
                    homeworkGrades.add(assignments.get(i).getGrade());
                    homeworkGradesWeight.add(assignments.get(i).getGradeWeight());
                } else if(assignments.get(i).instanceOf().equals(AssignmentType.QUIZ)){
                    quizGrades.add(assignments.get(i).getGrade());
                    quizGradesWeight.add(assignments.get(i).getGradeWeight());
                } else if(assignments.get(i).instanceOf().equals(AssignmentType.PROJECT)){
                    projectGrades.add(assignments.get(i).getGrade());
                    projectGradesWeight.add(assignments.get(i).getGradeWeight());
                } else if(assignments.get(i).instanceOf().equals(AssignmentType.EXAM)){
                    examGrades.add(assignments.get(i).getGrade());
                    examGradesWeight.add(assignments.get(i).getGradeWeight());
                } else if(assignments.get(i).instanceOf().equals(AssignmentType.FINAL)){
                    finalGrades.add(assignments.get(i).getGrade());
                    finalGradesWeight.add(assignments.get(i).getGradeWeight());
                }
            }

            gradeSystem = course.getGradeSystem();
            buildGrades();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        rebuildGradeList();
    }

    public void settingsGradeClickEvent(View view)
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void newEventGradeClickEvent(View view)
    {
        Intent intent;
        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        if(courseManager.getCourses().isEmpty()){
            intent = new Intent(this, AddNewCourseActivity.class);
        } else {
            intent = new Intent(this, AddNewEventActivity.class);
        }
        startActivityForResult(intent, REFRESH_LIST);
    }

    public static ArrayList<Assignment> getListOfAssignments() {
        return assignments;
    }

    public void checked(View view){
        buildGrades();
    }

    public void gradeClickEvent(View view){
        Spinner spinner = ((Spinner) findViewById(R.id.spinner));
        Course course = (Course) spinner.getItemAtPosition(spinner.getSelectedItemPosition());

        Intent intent = new Intent(this, EditCourseActivity.class);
        courseManager.setCurrentCourse(course);
        InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);
        startActivityForResult(intent, REFRESH_LIST);
    }

    public void calculateFinal(View view){
        Spinner spinner = ((Spinner) findViewById(R.id.spinner));
        Course course = (Course) spinner.getItemAtPosition(spinner.getSelectedItemPosition());

        Intent intent = new Intent(this, FinalActivity.class);
        courseManager.setCurrentCourse(course);
        InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);
        startActivity(intent);
    }

    private static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
