package com.roanuk.aceme;

import java.io.Serializable;

/**
 * Days of the week
 */
public enum Day implements Serializable {
    None,
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY;
}
