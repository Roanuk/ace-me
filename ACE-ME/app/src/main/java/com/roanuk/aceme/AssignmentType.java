package com.roanuk.aceme;

import java.io.Serializable;

/**
 * Valid types of assignments
 */
public enum AssignmentType implements Serializable {
    HOMEWORK,
    QUIZ,
    EXAM,
    PROJECT,
    FINAL;
}
