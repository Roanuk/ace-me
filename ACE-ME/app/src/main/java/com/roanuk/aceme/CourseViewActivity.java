package com.roanuk.aceme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class CourseViewActivity extends Activity {

    CourseManager courseManager;
    private static final int REFRESH_LIST = 0;
    private static ArrayList<Assignment> assignments;
    private CourseAdapter adapter;
    private UpcomingListAdapter adapter2;
    private boolean selected = false;
    private int selection = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());

        rebuildList();
    }

    public void rebuildList(){
        // before you display a layout, determine if the user has added any courses yet
        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        if (courseManager.getCourses().isEmpty()) {
            setContentView(R.layout.no_courses);
        } else {
            setContentView(R.layout.activity_course_view);

            ArrayList<Course> courses = courseManager.getCourses();
            adapter = new CourseAdapter(this, courses);

            Spinner spinner = ((Spinner) findViewById(R.id.spinnerCourses));

            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new Listener());

            if(selected){
                spinner.setSelection(selection);
            }
            else {
                selected = true;
                selection = 0;
            }

            Course course = (Course) spinner.getItemAtPosition(spinner.getSelectedItemPosition());

            rebuildCourseInfo(course);
        }
    }

    public void rebuildCourseInfo(Course course){
        TextView text = (TextView) findViewById(R.id.title);
        text.setText(course.toString());

        CourseTimes ct = course.getCourseTimes();
        ArrayList<CourseMeeting> cm = ct.getSortedMeetings();
        String meetingTime = "";

        for(CourseMeeting meeting: cm){
            String time = "";
            String time2 = "";
            boolean am = true;

            Day day = meeting.getDay();
            int shr = meeting.getStartHour();
            int sm = meeting.getStartMinute();
            int ehr = meeting.getEndHour();
            int em = meeting.getEndMinute();

            if(!meetingTime.equals("")){
                time += "\n";
            }

            if(day.equals(Day.MONDAY)){
                time += "M";
            } else if(day.equals(Day.TUESDAY)){
                time += "T";
            } else if(day.equals(Day.WEDNESDAY)){
                time += "W";
            } else if(day.equals(Day.THURSDAY)){
                time += "Th";
            } else if(day.equals(Day.FRIDAY)){
                time += "F";
            } else if(day.equals(Day.SATURDAY)){
                time += "Sat";
            } else if(day.equals(Day.SUNDAY)){
                time += "Sun";
            }

            if(shr > 12){ shr = shr - 12; }
            if(ehr > 12){
                ehr = ehr - 12;
                am = false;
            }

            String one = "" + shr;
            String two = "" + sm;
            String three = "" + ehr;
            String four = "" + em;

            if(two.length() == 1){
                two = "0" + sm;
            }
            if(four.length() == 1){
                four = "0" + em;
            }

            time2 += " " + one + ":" + two + "-" + three + ":" + four;

            if(am){ time2 += " AM"; }
            else{ time2 += " PM"; }

            meetingTime += time + time2;
        }

        text = (TextView) findViewById(R.id.time);
        text.setText(meetingTime);

        PriorityQueue<Assignment> a = new PriorityQueue<>();
        if (((CheckBox) findViewById(R.id.checkBox)).isChecked()) {
            a.addAll(course.getAssignments().getHomeworks());
        }
        if (((CheckBox) findViewById(R.id.checkBox2)).isChecked()) {
            a.addAll(course.getAssignments().getQuizzes());
        }
        if (((CheckBox) findViewById(R.id.checkBox3)).isChecked()) {
            a.addAll(course.getAssignments().getProjects());
        }
        if (((CheckBox) findViewById(R.id.checkBox4)).isChecked()) {
            a.addAll(course.getAssignments().getExams());
        }

        assignments = new ArrayList<>();
        int len = 10;
        if (len > a.size()) {
            len = a.size();
        }
        for (int i = 0; i < len; i++) {
            assignments.add(a.remove());
        }

        adapter2 = new UpcomingListAdapter(this, assignments);
        ListView listView = ((ListView) findViewById(R.id.upcomingList));
        listView.setAdapter(adapter2);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), EditAssignmentActivity.class);
                courseManager.setCurrentAssignment(CourseViewActivity.getListOfAssignments().get(position));
                InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);
                startActivityForResult(intent, REFRESH_LIST);
            }
        });
        setListViewHeightBasedOnChildren(listView);

        text = (TextView) findViewById(R.id.notes);
        if(course != null && course.getNotes() != null && course.getNotes() != ""){
            text.setText(course.getNotes());
        }
        else{
            text.setText("No notes at this time.");
        }
    }

    private class Listener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Spinner spinner = ((Spinner) findViewById(R.id.spinnerCourses));
            Course course = (Course) spinner.getItemAtPosition(spinner.getSelectedItemPosition());
            selection = spinner.getSelectedItemPosition();

            rebuildCourseInfo(course);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
            selection = courseManager.getCourses().size() - 1;
        }
        if(resultCode == RESULT_CANCELED){
            selected = false;
            selection = 0;
        }
        rebuildList();
    }

    public void settingsCourseClickEvent(View view)
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void newEventCourseClickEvent(View view)
    {
        Intent intent = new Intent(this, AddNewCourseActivity.class);
        startActivityForResult(intent, REFRESH_LIST);
    }

    public void allAssignmentsClickEvent(View view){
        Intent intent = new Intent(this,GradeViewActivity.class);
        startActivityForResult(intent, REFRESH_LIST);
    }

    public void editClickEvent(View view){
        Spinner spinner = ((Spinner) findViewById(R.id.spinnerCourses));
        Course course = (Course) spinner.getItemAtPosition(spinner.getSelectedItemPosition());

        Intent intent = new Intent(this, EditCourseActivity.class);
        courseManager.setCurrentCourse(course);
        InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);
        startActivityForResult(intent, REFRESH_LIST);
    }

    public void checkedClickEvent(View view){
        Spinner spinner = ((Spinner) findViewById(R.id.spinnerCourses));
        Course course = (Course) spinner.getItemAtPosition(spinner.getSelectedItemPosition());

        rebuildCourseInfo(course);
    }

    public static ArrayList<Assignment> getListOfAssignments() {
        return assignments;
    }

    private static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
