package com.roanuk.aceme;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

public class Alarms {
    private static ArrayList<PendingIntent> ActiveRepeats = new ArrayList<>();
    public static void RemindAssignment(Context context, Assignment assignment)
    {
        SharedPreferences eventPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        int assignmentAlertTime = 0;
        long mSecondsTillAlarm = assignment.getDueDate().getTime();
        alarmIntent.putExtra("title",assignment.getTitle());
        alarmIntent.putExtra("type",assignment.instanceOf());
        Recurrence recurrence = assignment.getRecurrence();
        long recurring = 0;
        if(recurrence.equals(Recurrence.EVERY_DAY))
        {
            recurring = 24*60*60*1000;
        }
        else if(recurrence.equals(Recurrence.EVERY_WEEK))
        {
            recurring = 7*24*60*60*1000;
        }
        else if(recurrence.equals(Recurrence.EVERY_TWO_WEEKS))
        {
            recurring = 2*7*24*60*60*1000;
        }
        else
        {
            recurring = 0;
        }
        if(assignment.instanceOf()==AssignmentType.EXAM || assignment.instanceOf()==AssignmentType.FINAL)
        {
            if(eventPrefs.getBoolean("pref_notifications_atTimeDue_checkbox2", false))
            {
                assignmentAlertTime=0;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your exam is "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_30MinBeforeDue_checkbox2", false))
            {
                assignmentAlertTime=1;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your exam is "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_hourBefore_checkbox2", false))
            {
                assignmentAlertTime=2;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your exam is "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_2HoursBefore_checkbox2", false))
            {
                assignmentAlertTime=3;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your exam is "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_onDueDay_checkbox2", false))
            {
                assignmentAlertTime=4;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your exam is "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_nightBeforeDue_checkbox2", false))
            {
                assignmentAlertTime=5;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your exam is "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_nightBeforeDue_checkbox2", false))
            {
                assignmentAlertTime=6;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your exam is "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
        }
        else
        {
            if(eventPrefs.getBoolean("pref_notifications_atTimeDue_checkbox1", false))
            {
                assignmentAlertTime=0;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your assignment is due "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_30MinBeforeDue_checkbox1", false))
            {
                assignmentAlertTime=1;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your assignment is due "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_hourBefore_checkbox1", false))
            {
                assignmentAlertTime=2;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your assignment is due "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_2HoursBefore_checkbox1", false))
            {
                assignmentAlertTime=3;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your assignment is due "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_onDueDay_checkbox1", false))
            {
                assignmentAlertTime=4;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your assignment is due "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_nightBeforeDue_checkbox1", false))
            {
                assignmentAlertTime=5;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your assignment is due "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
            if(eventPrefs.getBoolean("pref_notifications_nightBeforeDue_checkbox1", false))
            {
                assignmentAlertTime=6;
                mSecondsTillAlarm = getMSecondsTillAlarm(assignmentAlertTime, mSecondsTillAlarm);
                String whenDue = getWhenDue(assignmentAlertTime);
                alarmIntent.putExtra("message", "Your assignment is due "+whenDue);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, recurring, pendingIntent);
            }
        }
    }

    public static void RemindCourse(Context context, Course course)
    {
        SharedPreferences eventPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        long mSecondsBeforeRepeat = 7*24*60*60*1000;
        alarmIntent.putExtra("title",course.getCourseCode()+": "+course.getCourseTitle());

        if(System.currentTimeMillis()>course.getStartDate().getTime() && System.currentTimeMillis()<course.getEndDate().getTime())
        {
            long mSecondsTillAlarm =0;
            int courseAlertTime = 0;
            Calendar current = Calendar.getInstance();
            Iterator<CourseMeeting> meetingIterator = course.getCourseTimes().getMeetings().iterator();
            while(meetingIterator.hasNext()) {
                CourseMeeting currentCourseMeeting = meetingIterator.next();
                mSecondsTillAlarm = (currentCourseMeeting.getCalendarDay() - current.get(Calendar.DAY_OF_WEEK)) * 24 * 60 * 60 * 1000;
                mSecondsTillAlarm += currentCourseMeeting.getStartHour() * 60 * 60 * 1000;
                mSecondsTillAlarm += currentCourseMeeting.getStartMinute() * 60 * 1000;
                if(mSecondsTillAlarm>0)
                {
                    current.set(current.get(Calendar.YEAR),current.get(Calendar.MONTH),current.get(Calendar.DAY_OF_MONTH),0,0,0);
                    mSecondsTillAlarm+=current.getTimeInMillis();
                    if(eventPrefs.getBoolean("pref_notifications_atTimeDue_checkbox3", false))
                    {
                        courseAlertTime=0;
                        String whenDue = getWhenDue(courseAlertTime);
                        mSecondsTillAlarm = getMSecondsTillAlarm(courseAlertTime, mSecondsTillAlarm);
                        alarmIntent.putExtra("message","Your class is starting "+whenDue);
                        alarmIntent.putExtra("isCourse",true);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, mSecondsBeforeRepeat, pendingIntent);
                        ActiveRepeats.add(pendingIntent);
                    }
                    if(eventPrefs.getBoolean("pref_notifications_30MinBeforeDue_checkbox3", false))
                    {
                        courseAlertTime=1;
                        String whenDue = getWhenDue(courseAlertTime);
                        mSecondsTillAlarm = getMSecondsTillAlarm(courseAlertTime, mSecondsTillAlarm);
                        alarmIntent.putExtra("message","Your class is starting "+whenDue);
                        alarmIntent.putExtra("isCourse",true);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, mSecondsBeforeRepeat, pendingIntent);
                        ActiveRepeats.add(pendingIntent);
                    }
                    if(eventPrefs.getBoolean("pref_notifications_hourBefore_checkbox3", false))
                    {
                        courseAlertTime=2;
                        String whenDue = getWhenDue(courseAlertTime);
                        mSecondsTillAlarm = getMSecondsTillAlarm(courseAlertTime, mSecondsTillAlarm);
                        alarmIntent.putExtra("message","Your class is starting "+whenDue);
                        alarmIntent.putExtra("isCourse",true);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, mSecondsBeforeRepeat, pendingIntent);
                        ActiveRepeats.add(pendingIntent);
                    }
                    if(eventPrefs.getBoolean("pref_notifications_2HoursBefore_checkbox3", false))
                    {
                        courseAlertTime=3;
                        String whenDue = getWhenDue(courseAlertTime);
                        mSecondsTillAlarm = getMSecondsTillAlarm(courseAlertTime, mSecondsTillAlarm);
                        alarmIntent.putExtra("message","Your class is starting "+whenDue);
                        alarmIntent.putExtra("isCourse",true);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, mSecondsBeforeRepeat, pendingIntent);
                        ActiveRepeats.add(pendingIntent);
                    }
                    if(eventPrefs.getBoolean("pref_notifications_onDueDay_checkbox3", false))
                    {
                        courseAlertTime=4;
                        String whenDue = getWhenDue(courseAlertTime);
                        mSecondsTillAlarm = getMSecondsTillAlarm(courseAlertTime, mSecondsTillAlarm);
                        alarmIntent.putExtra("message","Your class is starting "+whenDue);
                        alarmIntent.putExtra("isCourse",true);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, mSecondsBeforeRepeat, pendingIntent);
                        ActiveRepeats.add(pendingIntent);
                    }
                    if(eventPrefs.getBoolean("pref_notifications_nightBeforeDue_checkbox3", false))
                    {
                        courseAlertTime=5;
                        String whenDue = getWhenDue(courseAlertTime);
                        mSecondsTillAlarm = getMSecondsTillAlarm(courseAlertTime, mSecondsTillAlarm);
                        alarmIntent.putExtra("message","Your class is starting "+whenDue);
                        alarmIntent.putExtra("isCourse",true);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, mSecondsBeforeRepeat, pendingIntent);
                        ActiveRepeats.add(pendingIntent);
                    }
                    if(eventPrefs.getBoolean("pref_notifications_nightBeforeDue_checkbox3", false))
                    {
                        courseAlertTime=6;
                        String whenDue = getWhenDue(courseAlertTime);
                        mSecondsTillAlarm = getMSecondsTillAlarm(courseAlertTime, mSecondsTillAlarm);
                        alarmIntent.putExtra("message","Your class is starting "+whenDue);
                        alarmIntent.putExtra("isCourse",true);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mSecondsTillAlarm, mSecondsBeforeRepeat, pendingIntent);
                        ActiveRepeats.add(pendingIntent);
                    }

                }
            }
        }
    }

    public static void removeRepeatAlarm(Context context, int index)
    {
        ((AlarmManager) context.getSystemService(context.ALARM_SERVICE)).cancel(ActiveRepeats.remove(index));
    }

    public static boolean removeRepeatAlarm(Context context, PendingIntent pendingIntent)
    {
        ((AlarmManager) context.getSystemService(context.ALARM_SERVICE)).cancel(pendingIntent);
        return ActiveRepeats.remove(pendingIntent);
    }

    public static void removeRepeatAlarms(Context context)
    {
        Iterator<PendingIntent> iterator = ActiveRepeats.iterator();
        while(iterator.hasNext()) {
            ((AlarmManager) context.getSystemService(context.ALARM_SERVICE)).cancel(iterator.next());
        }
    }

    private static long getMSecondsTillAlarm(int assignmentAlertTime, long mSecondsTillAlarm) {
        switch(assignmentAlertTime) {
            case 0: //AtTime
                break;

            case 1: //30Min
                mSecondsTillAlarm += (30*60*1000);
                break;            //min,sec,msec

            case 2: //1hr
                mSecondsTillAlarm += (1*60*60*1000);
                break;           //hr,min,sec,msec

            case 3: //2hr
                mSecondsTillAlarm += (2*60*60*1000);
                break;

            case 4: //OnDay //Unfinished
                mSecondsTillAlarm += (1000);
                break;

            case 5: //NightBefore //Unfinished
                mSecondsTillAlarm += (1000);
                break;

            case 6: //DayBefore //Unfinished
                mSecondsTillAlarm += (1000);
                break;

            default:
                break;

        }
        return mSecondsTillAlarm;
    }

    private static String getWhenDue(int eventAlertTime) {
        String whenDue;
        switch(eventAlertTime) {
            case 0: //AtTime
                whenDue = "now.";
                break;

            case 1: //30Min
                whenDue = "in 30 minutes.";
                break;            //min,sec,msec

            case 2: //1hr
                whenDue = "in 1 hour.";
                break;           //hr,min,sec,msec

            case 3: //2hr
                whenDue = "in 2 hours.";
                break;

            case 4: //OnDay //Unfinished
                whenDue = "";
                break;

            case 5: //NightBefore //Unfinished
                whenDue = "";
                break;

            case 6: //DayBefore //Unfinished
                whenDue = "";
                break;

            default:
                whenDue = "now.";
                break;

        }
        return whenDue;
    }
}
