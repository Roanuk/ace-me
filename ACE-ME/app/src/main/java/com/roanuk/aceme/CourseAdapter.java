package com.roanuk.aceme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

public class CourseAdapter extends ArrayAdapter<Course> implements Serializable {
    public CourseAdapter(Context context, ArrayList<Course> assignment) {
        super(context, 0, assignment);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Course assignment = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_grade_layout, parent, false);
        }
        // Lookup view for data population
        TextView tvCourse = (TextView) convertView.findViewById(R.id.Course);
        // Populate the data into the template view using the data object
        tvCourse.setText(assignment.getCourseTitle());
        // Return the completed view to render on screen
        return convertView;
    }
}