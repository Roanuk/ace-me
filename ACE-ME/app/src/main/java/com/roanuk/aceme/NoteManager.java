package com.roanuk.aceme;

import java.io.Serializable;
import java.util.ArrayList;

public class NoteManager implements Serializable{

    public static String KEY = "5";
    ArrayList<AgendaNote> notes;

    public NoteManager(){
        notes = new ArrayList<>();
    }

    public void addNote(AgendaNote note){
        notes.add(note);
    }

    public void removeNote(int i){
        notes.remove(i);
    }

    public ArrayList<AgendaNote> getNotes(){
        return notes;
    }
}
