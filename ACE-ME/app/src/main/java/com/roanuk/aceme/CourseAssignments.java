package com.roanuk.aceme;

import android.widget.Toast;

import java.io.Serializable;
import java.util.PriorityQueue;

/**
 * Created by Bryson on 4/1/2015.
 */
public class CourseAssignments implements Serializable {

    PriorityQueue<Assignment> assignments;
    PriorityQueue<Assignment> homeworks;
    PriorityQueue<Assignment> quizzes;
    PriorityQueue<Assignment> exams;
    PriorityQueue<Assignment> projects;
    PriorityQueue<Assignment> finals;

    public CourseAssignments(){
        assignments = new PriorityQueue<>();
        homeworks = new PriorityQueue<>();
        quizzes = new PriorityQueue<>();
        exams = new PriorityQueue<>();
        projects = new PriorityQueue<>();
        finals = new PriorityQueue<>();
    }

    /**
     * method used to add an assignment to the list of course assignments
     * @param assignment assignment to be added to list
     */
    public void add(Assignment assignment){
        assignments.add(assignment);

        AssignmentType type = assignment.instanceOf();
        if(type.equals(AssignmentType.HOMEWORK)){
            homeworks.add(assignment);
        } else if(type.equals(AssignmentType.QUIZ)){
            quizzes.add(assignment);
        } else if(type.equals(AssignmentType.EXAM)){
            exams.add(assignment);
        } else if(type.equals(AssignmentType.PROJECT)){
            projects.add(assignment);
        } else if(type.equals(AssignmentType.FINAL)){
            finals.add(assignment);
        }
    }

    /**
     * method used to remove an assignment from the list
     * @param assignment the assignment to be removed
     */
    public boolean remove(Assignment assignment) {
        if(assignments.contains(assignment))
            assignments.remove(assignment);
        if(homeworks.contains(assignment)) {
            homeworks.remove(assignment);
            return true;
        }
        if(quizzes.contains(assignment)) {
            quizzes.remove(assignment);
            return true;
        }
        if(exams.contains(assignment)) {
            exams.remove(assignment);
            return true;
        }
        if(projects.contains(assignment)) {
            projects.remove(assignment);
            return true;
        }
        if(finals.contains(assignment)) {
            finals.remove(assignment);
            return true;
        }
        return false;
    }

/*    public void replaceAssignment(Assignment a, Assignment b){
        if(assignments.contains(a))
            assignments.
        if(homeworks.contains(assignment))
            homeworks.remove(assignment);
        if(quizzes.contains(assignment))
            quizzes.remove(assignment);
        if(exams.contains(assignment))
            exams.remove(assignment);
        if(projects.contains(assignment))
            projects.remove(assignment);
        if(finals.contains(assignment))
            finals.remove(assignment);
    }        */

    /**
     * method used to get a priority queue of all the assignments
     * @return the assignments
     */
    public PriorityQueue<Assignment> getCourseAssignments(){
        return assignments;
    }

    /**
     * method used to get a priority queue of all the homeworks
     * @return the homeworks
     */
    public PriorityQueue<Assignment> getHomeworks(){
        return homeworks;
    }

    /**
     * method used to get a priority queue of all the quizzes
     * @return the quizzes
     */
    public PriorityQueue<Assignment> getQuizzes(){
        return quizzes;
    }

    /**
     * method used to get a priority queue of all the exams
     * @return the exams
     */
    public PriorityQueue<Assignment> getExams(){
        return exams;
    }

    /**
     * method used to get a priority queue of all the projects
     * @return the projects
     */
    public PriorityQueue<Assignment> getProjects(){
        return projects;
    }

    /**
     * method used to get a priority queue of all the finals
     * @return the finals
     */
    public PriorityQueue<Assignment> getFinals(){
        return finals;
    }
}
