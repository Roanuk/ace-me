package com.roanuk.aceme;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * class used to manage a user's courses and associated assignments
 */
public class CourseManager implements Serializable{

    public static String KEY = "10";
    private ArrayList<Course> courses = new ArrayList<>();
    private Course currentCourse = null;
    private Assignment currentAssignment = null;

    /**
     * method used to set up a new course
     */
    public void addNewCourse(){
        Course c = CourseFactory.createCourse();
        courses.add(c);
    }

    /**
     * method used to set up a new course with parameters
     */
    public void addNewCourse(String title, String courseCode, Date start, Date end,
                          CourseTimes times, String notes, String prof,
                          Priority priority, String syllabus, GradeSystem gradeSystem){
        Course c = CourseFactory.createCourse(title, courseCode, start, end, times, notes,
                                   prof, priority, syllabus, gradeSystem);
        courses.add(c);
    }

    public void addCourse(Course c){
        courses.add(c);
    }

    /**
     * method used to get the course indexed at i
     * @param i the index of the course you want
     * @return the course
     */
    public Course getCourse(int i){
        return courses.get(i);
    }

    /**
     * method used to get the user's courses
     * @return the array list of courses
     */
    public ArrayList<Course> getCourses(){
        return courses;
    }

    public Course getCurrentCourse() {
        return currentCourse;
    }

    public void setCurrentCourse(Course currentCourse) {
        this.currentCourse = currentCourse;
    }

    public Assignment getCurrentAssignment() {
        return currentAssignment;
    }

    public void setCurrentAssignment(Assignment currentAssignment) {
        this.currentAssignment = currentAssignment;
    }

    public void removeCourse(Course c){
        if(courses.contains(c)){
            courses.remove(c);
        }
    }

    public ArrayList<Assignment> getUpcomingAssignments(Date date) {
        ArrayList<Assignment> assignments = new ArrayList<>();
        for (Course course : courses) {
            CourseAssignments ca = course.getAssignments();
            PriorityQueue<Assignment> a = new PriorityQueue<>(ca.getCourseAssignments());
            while (!a.isEmpty()) {
                Assignment assignment = a.remove();
                Date dueDate = assignment.getDueDate();
                Calendar cal1 = Calendar.getInstance();
                Calendar cal2 = Calendar.getInstance();
                cal1.setTime(date);
                cal2.setTime(dueDate);
                int month1 = cal1.get(Calendar.MONTH);
                int day1 = cal1.get(Calendar.DAY_OF_MONTH);
                int year1 = cal1.get(Calendar.YEAR);
                int month2 = cal2.get(Calendar.MONTH);
                int day2 = cal2.get(Calendar.DAY_OF_MONTH);
                int year2 = cal2.get(Calendar.YEAR);
                if(year2 > year1){
                    assignments.add(assignment);
                }
                else if(year2 == year1 && month2 > month1){
                    assignments.add(assignment);
                }
                else if(year1 == year2 && month2 == month1 && day2 >= day1){
                    assignments.add(assignment);
                }
            }
        }
        return assignments;
    }

}
