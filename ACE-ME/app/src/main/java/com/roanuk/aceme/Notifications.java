package com.roanuk.aceme;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import java.util.NoSuchElementException;
import java.util.PriorityQueue;

public class Notifications {

    private static PriorityQueue<Integer> NotificationsQueue = new PriorityQueue<>();


    public static void CreateNew(Context context, Intent intent)
    {
        Intent resultIntent;
        TaskStackBuilder stackBuilder;
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(intent.getStringExtra("title"))
                        .setContentText(intent.getStringExtra("message"));

        stackBuilder = TaskStackBuilder.create(context);
        AssignmentType type = (AssignmentType)intent.getSerializableExtra("type");
        if (intent.getBooleanExtra("isCourse",false))
        {
            resultIntent = new Intent(context, CourseViewActivity.class);
            stackBuilder.addParentStack(CourseViewActivity.class);
        }
        else if(type==AssignmentType.EXAM || type==AssignmentType.FINAL)
        {
            resultIntent = new Intent(context, TestViewActivity.class);
            stackBuilder.addParentStack(TestViewActivity.class);
        }
        else if(type==AssignmentType.HOMEWORK||type==AssignmentType.QUIZ||type==AssignmentType.PROJECT)
        {
            resultIntent = new Intent(context, AssignmentViewActivity.class);
            stackBuilder.addParentStack(AssignmentViewActivity.class);
        }
        else
        {
            resultIntent = new Intent(context, HomeScreenActivity.class);
            stackBuilder.addParentStack(HomeScreenActivity.class);
        }
        resultIntent.putExtra("notificationNumber",NotificationsQueue.size());
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setAutoCancel(true); //automatically cancels the notification
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NotificationsQueue.size(), mBuilder.build());
        NotificationsQueue.add(NotificationsQueue.size());
    }

    public static void ClearAll(Context context)
    {
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        while (!NotificationsQueue.isEmpty()) {
            mNotificationManager.cancel(NotificationsQueue.poll());
        }
    }

    public static boolean ClearOne(Context context, byte toCancel)
    {
        boolean returner = false;
        if(toCancel>=0 && NotificationsQueue.remove(new Integer(toCancel)))
        {
            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.cancel(toCancel);
            returner = true;
        }
        return returner;
    }
}
