package com.roanuk.aceme;

/**
 * Created by Bryson on 4/1/2015.
 */
public interface Editable {

    /**
     * Any object that implements the Editable class should have an implemented
     * edit() method that edits something within the object.
     */
    public void edit();

}
