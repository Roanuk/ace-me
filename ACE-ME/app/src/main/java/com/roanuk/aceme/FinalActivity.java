package com.roanuk.aceme;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.PriorityQueue;


public class FinalActivity extends ActionBarActivity {

    CourseManager courseManager;
    private Course currentCourse;
    private GradeSystem gradeSystem;
    private static ArrayList<Assignment> assignments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);

        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());

        currentCourse = courseManager.getCurrentCourse();

        gradeSystem = currentCourse.getGradeSystem();

        rebuild();
    }

    public void rebuild(){
        ((TextView) findViewById(R.id.textView10)).setText(currentCourse.toString());
        if(currentCourse.getGradeGoal() != 0){
            ((EditText) findViewById(R.id.editText)).setText("" + currentCourse.getGradeGoal());
        }
        else{
            ((EditText) findViewById(R.id.editText)).setText("100.0");
        }
        double weight1 = gradeSystem.getHomeworkWeight();
        double weight2 = gradeSystem.getQuizWeight();
        double weight3 = gradeSystem.getProjectWeight();
        double weight4 = gradeSystem.getExamWeight();
        double weight5 = gradeSystem.getFinalWeight();
        ((EditText) findViewById(R.id.finalOne)).setText("" + weight1);
        ((EditText) findViewById(R.id.finalTwo)).setText("" + weight2);
        ((EditText) findViewById(R.id.finalThree)).setText("" + weight3);
        ((EditText) findViewById(R.id.finalFour)).setText("" + weight4);
        ((EditText) findViewById(R.id.finalFive)).setText("" + weight5);

        ((TextView) findViewById(R.id.textView55)).setText("-");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_add_new_course, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        ArrayList<Course> courses = null;
        courses = ((CourseManager) InternalStorage.readObject(getApplicationContext())).getCourses();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_new_event) {
            if (courses.isEmpty()) {
                Toast.makeText(getApplicationContext(), "You need a course to do that!", Toast.LENGTH_LONG).show();
            } else {
                Intent intent = new Intent(this, AddNewEventActivity.class);
                startActivity(intent);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void calculate(View view){
        ArrayList<Double> homeworkGrades = new ArrayList<>();
        ArrayList<Double> homeworkGradesWeight = new ArrayList<>();
        ArrayList<Double> quizGrades = new ArrayList<>();
        ArrayList<Double> quizGradesWeight = new ArrayList<>();
        ArrayList<Double> projectGrades = new ArrayList<>();
        ArrayList<Double> projectGradesWeight = new ArrayList<>();
        ArrayList<Double> examGrades = new ArrayList<>();
        ArrayList<Double> examGradesWeight = new ArrayList<>();
        ArrayList<Double> finalGrades = new ArrayList<>();
        ArrayList<Double> finalGradesWeight = new ArrayList<>();
        PriorityQueue<Assignment> a = currentCourse.getAssignments().getCourseAssignments();
        assignments = new ArrayList<>(a);
        for(int i = 0; i < assignments.size(); i++){
            if(assignments.get(i).instanceOf().equals(AssignmentType.HOMEWORK)){
                homeworkGrades.add(assignments.get(i).getGrade());
                homeworkGradesWeight.add(assignments.get(i).getGradeWeight());
            } else if(assignments.get(i).instanceOf().equals(AssignmentType.QUIZ)){
                quizGrades.add(assignments.get(i).getGrade());
                quizGradesWeight.add(assignments.get(i).getGradeWeight());
            } else if(assignments.get(i).instanceOf().equals(AssignmentType.PROJECT)){
                projectGrades.add(assignments.get(i).getGrade());
                projectGradesWeight.add(assignments.get(i).getGradeWeight());
            } else if(assignments.get(i).instanceOf().equals(AssignmentType.EXAM)){
                examGrades.add(assignments.get(i).getGrade());
                examGradesWeight.add(assignments.get(i).getGradeWeight());
            } else if(assignments.get(i).instanceOf().equals(AssignmentType.FINAL)){
                finalGrades.add(assignments.get(i).getGrade());
                finalGradesWeight.add(assignments.get(i).getGradeWeight());
            }
        }

        Double goal = Double.parseDouble(((EditText) findViewById(R.id.editText)).getText().toString());

        double total1 = 0;
        double total2 = 0;
        double total3 = 0;
        double total4 = 0;
        double total5 = 0;

        double zero1 = 0;
        double zero2 = 0;
        double zero3 = 0;
        double zero4 = 0;
        double zero5 = 0;
        for(int i = 0; i < homeworkGrades.size(); i++){
            total1 += (homeworkGrades.get(i) * homeworkGradesWeight.get(i));
            zero1 += homeworkGradesWeight.get(i);
        }
        for(int i = 0; i < quizGrades.size(); i++){
            total2 += (quizGrades.get(i) * quizGradesWeight.get(i));
            zero2 += quizGradesWeight.get(i);
        }
        for(int i = 0; i < projectGrades.size(); i++){
            total3 += (projectGrades.get(i) * projectGradesWeight.get(i));
            zero3 += projectGradesWeight.get(i);
        }
        for(int i = 0; i < examGrades.size(); i++){
            total4 += (examGrades.get(i) * examGradesWeight.get(i));
            zero4 += examGradesWeight.get(i);
        }
        for(int i = 0; i < finalGrades.size(); i++){
            total5 += (finalGrades.get(i) * finalGradesWeight.get(i));
            zero5 += finalGradesWeight.get(i);
        }
        if(!homeworkGrades.isEmpty()) {
            total1 = total1 / zero1;
        }
        if(!quizGrades.isEmpty()) {
            total2 = total2 / zero2;
        }
        if(!projectGrades.isEmpty()) {
            total3 = total3 / zero3;
        }
        if(!examGrades.isEmpty()) {
            total4 = total4 / zero4;
        }
        if(!finalGrades.isEmpty()) {
            total5 = total5 / zero5;
        }

        double weight1 = gradeSystem.getHomeworkWeight();
        double weight2 = gradeSystem.getQuizWeight();
        double weight3 = gradeSystem.getProjectWeight();
        double weight4 = gradeSystem.getExamWeight();
        double weight5 = gradeSystem.getFinalWeight();

        double average = (total1*(weight1/100))
                + (total2*(weight2/100))
                + (total3*(weight3/100))
                + (total4*(weight4/100));

        goal = goal - average;
        goal = (goal / weight5) * 100;

        int index = (Double.toString(goal)).indexOf(".") + 2;

        ((TextView) findViewById(R.id.textView55)).setText("" + (Double.toString(goal)).substring(0, index) + "%");

        Double myGoal = Double.parseDouble(((EditText) findViewById(R.id.editText)).getText().toString());

        currentCourse.setGradeGoal(myGoal);

        //courseManager.removeCourse(currentCourse);
       // courseManager.addCourse(currentCourse);
        //courseManager.setCurrentCourse(currentCourse);
        int i = courseManager.getCourses().indexOf(currentCourse);
        courseManager.getCourse(i).setGradeGoal(myGoal);
        courseManager.removeCourse(currentCourse);
        courseManager.addCourse(currentCourse);

        InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);
    }
}
