package com.roanuk.aceme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;

public class TodayCourseAdapter extends ArrayAdapter<Course> implements Serializable {
    public TodayCourseAdapter(Context context, ArrayList<Course> course) {
        super(context, 0, course);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Course course = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.course_info_layout, parent, false);
        }
        // Lookup view for data population
        TextView courseText = (TextView) convertView.findViewById(R.id.textView34);
        TextView priorityText = (TextView) convertView.findViewById(R.id.textView35);
        // Populate the data into the template view using the data object
        courseText.setText(course.toString());

        ArrayList<CourseMeeting> today = new ArrayList<>();
        int currentDay = AgendaActivity.getCurrentDayOfWeek();
        ArrayList<CourseMeeting> meetings = course.getCourseTimes().getMeetings();
        for(CourseMeeting meeting: meetings){
            if(meeting.getDay().equals(Day.SUNDAY) && currentDay == 1){
                today.add(meeting);
            } else
            if(meeting.getDay().equals(Day.MONDAY) && currentDay == 2){
                today.add(meeting);
            } else
            if(meeting.getDay().equals(Day.TUESDAY) && currentDay == 3){
                today.add(meeting);
            } else
            if(meeting.getDay().equals(Day.WEDNESDAY) && currentDay == 4){
                today.add(meeting);
            } else
            if(meeting.getDay().equals(Day.THURSDAY) && currentDay == 5){
                today.add(meeting);
            } else
            if(meeting.getDay().equals(Day.FRIDAY) && currentDay == 6){
                today.add(meeting);
            } else
            if(meeting.getDay().equals(Day.SATURDAY) && currentDay == 7){
                today.add(meeting);
            }

        }

        String time = "";
        for(CourseMeeting meeting: today){

            boolean am = true;
            int startHour = 0;
            startHour = meeting.getStartHour();
            if(meeting.getStartHour() > 12){
                am = false;
                startHour -= 12;
            }
            if(today.indexOf(meeting) != 0)
            {
                time += "\n";
            }
            if(meeting.getStartMinute() <= 9){
                time += startHour + ":0" + meeting.getStartMinute();
            } else {
                time += startHour + ":" + meeting.getStartMinute();
            }
            if(am){
                time += " AM";
            } else {
                time += " PM";
            }
        }

        priorityText.setText(time);

        return convertView;
    }
}