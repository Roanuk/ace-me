package com.roanuk.aceme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class TestViewActivity extends Activity {

    CourseManager courseManager;
    private static final int REFRESH_LIST = 0;
    private static ArrayList<Assignment> listOfAssignments;
    private AssignmentsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());

        rebuildTestList();
    }

    /* refreshes the test list */
    public void rebuildTestList() {
        // before you display a layout, determine if the user has added any courses yet
        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        if (courseManager.getCourses().isEmpty()) {
            setContentView(R.layout.no_test);
        } else {
            setContentView(R.layout.activity_tests_screen);

            ArrayList<Course> courses = courseManager.getCourses();
            PriorityQueue<Assignment> allTest = new PriorityQueue<>();

            for (int i = 0; i < courses.size(); i++) {
                Course course = courses.get(i);
                PriorityQueue<Assignment> exams = course.getAssignments().getExams();
                PriorityQueue<Assignment> finals = course.getAssignments().getFinals();
                allTest.addAll(exams);
                allTest.addAll(finals);
            }

            listOfAssignments = new ArrayList<>();
            while(!allTest.isEmpty()){
                listOfAssignments.add(allTest.poll());
            }
            adapter = new AssignmentsAdapter(this, listOfAssignments);

            ListView listView = (ListView) findViewById(R.id.assignmentsList);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(view.getContext(), EditAssignmentActivity.class);
                    courseManager.setCurrentAssignment(TestViewActivity.getListOfAssignments().get(position));
                    InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);
                    startActivityForResult(intent, REFRESH_LIST);
                }
            });
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        rebuildTestList();
    }

    public void settingsClickEventTest(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void newEventClickEventTest(View view) {
        Intent intent;
        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        if(courseManager.getCourses().isEmpty()){
            intent = new Intent(this, AddNewCourseActivity.class);
        } else {
            intent = new Intent(this, AddNewEventActivity.class);
            intent.putExtra("isTest", true);
        }
        startActivityForResult(intent, REFRESH_LIST);
    }

    public static ArrayList<Assignment> getListOfAssignments() {
        return listOfAssignments;
    }
}