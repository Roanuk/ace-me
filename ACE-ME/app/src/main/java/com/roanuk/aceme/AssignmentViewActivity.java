package com.roanuk.aceme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * this activity displays a list of all the assignments
 */
public class AssignmentViewActivity extends Activity {

    CourseManager courseManager;
    private static final int REFRESH_LIST = 0;
    private static ArrayList<Assignment> listOfAssignments;
    private AssignmentsAdapter adapter;
    private static int currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());

        rebuild();
    }

    public void rebuild(){
        setContentView(R.layout.activity_assignments_screen);
        currentLayout = R.layout.activity_assignments_screen;
        rebuildAssignmentList();
    }

    /**
     * refreshes the assignments list
     */
    public void rebuildAssignmentList() {
        // before you display a layout, determine if the user has added any courses yet
        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        if (courseManager.getCourses().isEmpty()) {
            setContentView(R.layout.no_assignments);
            currentLayout = R.layout.no_assignments;
        } else {
            //setContentView(R.layout.activity_assignments_screen);
            //currentLayout = R.layout.activity_assignments_screen;

            ArrayList<Course> courses = courseManager.getCourses();
            PriorityQueue<Assignment> allAssignments = new PriorityQueue<>();

            for (int i = 0; i < courses.size(); i++) {
                Course course = courses.get(i);
                PriorityQueue<Assignment> homeworks = course.getAssignments().getHomeworks();
                PriorityQueue<Assignment> quizzes = course.getAssignments().getQuizzes();
                PriorityQueue<Assignment> projects = course.getAssignments().getProjects();
                if (((CheckBox) findViewById(R.id.homeworks)).isChecked()) {
                    allAssignments.addAll(homeworks);
                }
                if (((CheckBox) findViewById(R.id.quizzes)).isChecked()) {
                    allAssignments.addAll(quizzes);
                }
                if (((CheckBox) findViewById(R.id.projects)).isChecked()) {
                    allAssignments.addAll(projects);
                }
            }

            listOfAssignments = new ArrayList<>();
            while(!allAssignments.isEmpty()){
                listOfAssignments.add(allAssignments.poll());
            }

            adapter = new AssignmentsAdapter(this, listOfAssignments);

            ListView listView = (ListView) findViewById(R.id.assignmentsList);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(view.getContext(), EditAssignmentActivity.class);
                    courseManager.setCurrentAssignment(AssignmentViewActivity.getListOfAssignments().get(position));
                    InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);
                    startActivityForResult(intent, REFRESH_LIST);
                }
            });
        }
    }

    /* called when started activity returns to this activity */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        rebuild();
    }

    public void assignmentClickEvent(View view) {
        rebuildAssignmentList();
    }

    public void settingsClickEvent(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void newEventClickEvent(View view) {
        Intent intent;
        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        if(courseManager.getCourses().isEmpty()){
            intent = new Intent(this, AddNewCourseActivity.class);
        } else {
            intent = new Intent(this, AddNewEventActivity.class);
        }
        startActivityForResult(intent, REFRESH_LIST);
    }

    public static ArrayList<Assignment> getListOfAssignments() {
        return listOfAssignments;
    }
}