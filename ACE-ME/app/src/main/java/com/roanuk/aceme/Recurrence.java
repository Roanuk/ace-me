package com.roanuk.aceme;

/**
 * Created by coltonchilders on 5/2/15.
 */
public enum Recurrence {
    NONE("None"),
    EVERY_DAY("Every Day"),
    EVERY_CLASS("Every Class"),
    EVERY_WEEK("Every Week"),
    EVERY_TWO_WEEKS("Every Two Weeks");

    private String name;

    Recurrence(String name) { this.name = name; }

    @Override
    public String toString() { return name; }
}
