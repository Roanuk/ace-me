package com.roanuk.aceme;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class AddNewCourseActivity extends ActionBarActivity {

    private CourseTimes ct = new CourseTimes();
    public Calendar cal;
    public int beginday;
    public int beginmonth;
    public int beginyear;
    public int endday;
    public int endmonth;
    public int endyear;
    public int starthour;
    public int startminute;
    public int endhour;
    public int endminute;
    public static final SimpleDateFormat DATE_FORMAT =
            new SimpleDateFormat("MM/dd/yyyy", Locale.US);
    public static final SimpleDateFormat TIME_FORMAT =
            new SimpleDateFormat("h:mm a", Locale.US);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_course);

        rebuildList();

        cal = Calendar.getInstance(TimeZone.getDefault());
        beginday = cal.get(Calendar.DAY_OF_MONTH);
        beginmonth = cal.get(Calendar.MONTH);
        beginyear = cal.get(Calendar.YEAR);
        endday = cal.get(Calendar.DAY_OF_MONTH);
        endmonth = cal.get(Calendar.MONTH);
        endyear = cal.get(Calendar.YEAR);
        ((TextView)findViewById(R.id.beginDate)).setText(AddNewCourseActivity.DATE_FORMAT.format(cal.getTime()));
        ((TextView)findViewById(R.id.endDate)).setText(AddNewCourseActivity.DATE_FORMAT.format(cal.getTime()));

        starthour = cal.get(Calendar.HOUR_OF_DAY);
        startminute  = cal.get(Calendar.MINUTE);
        endhour = cal.get(Calendar.HOUR_OF_DAY);
        endminute  = cal.get(Calendar.MINUTE);
        ((TextView)findViewById(R.id.startTime)).setText(AddNewCourseActivity.TIME_FORMAT.format(cal.getTime()));
        ((TextView)findViewById(R.id.endTime)).setText(AddNewCourseActivity.TIME_FORMAT.format(cal.getTime()));
    }

    public void rebuildList(){
        final ArrayList<CourseMeeting> meetings = ct.getMeetings();

        CourseTimeAdapter adapter = new CourseTimeAdapter(this, meetings);
        ListView listView = (ListView) findViewById(R.id.listView2);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showRemovePopUp(meetings.get(position));
            }
        });

    }

    public void setDate1(View view) {
        cal = Calendar.getInstance(TimeZone.getDefault());
        DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                beginday = d;
                beginmonth = m;
                beginyear = y;
                cal.set(Calendar.DAY_OF_MONTH, beginday);
                cal.set(Calendar.MONTH, beginmonth);
                cal.set(Calendar.YEAR, beginyear);
                ((TextView)findViewById(R.id.beginDate)).setText(AddNewCourseActivity.DATE_FORMAT.format(cal.getTime()));
            }
        }, this.beginyear, this.beginmonth, this.beginday);
        datePicker.setCancelable(false);
        datePicker.setTitle("Set Beginning Date...");
        datePicker.show();
    }

    public void setDate2(View view) {
        cal = Calendar.getInstance(TimeZone.getDefault());
        DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                endday = d;
                endmonth = m;
                endyear = y;
                cal.set(Calendar.DAY_OF_MONTH, endday);
                cal.set(Calendar.MONTH, endmonth);
                cal.set(Calendar.YEAR, endyear);
                ((TextView)findViewById(R.id.endDate)).setText(AddNewCourseActivity.DATE_FORMAT.format(cal.getTime()));
            }
        }, this.endyear, this.endmonth, this.endday);
        datePicker.setCancelable(false);
        datePicker.setTitle("Set End Date...");
        datePicker.show();
    }

    public void setTime1(View view) {
        cal = Calendar.getInstance(TimeZone.getDefault());
        TimePickerDialog timePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int h, int m) {
                starthour = h;
                startminute = m;
                cal.set(Calendar.HOUR_OF_DAY, starthour);
                cal.set(Calendar.MINUTE, startminute);
                ((TextView) findViewById(R.id.startTime)).setText(AddNewCourseActivity.TIME_FORMAT.format(cal.getTime()));
            }
        }, this.starthour, this.startminute, true);
        timePicker.setCancelable(false);
        timePicker.setTitle("Set Start Time...");
        timePicker.show();
    }

    public void setTime2(View view) {
        cal = Calendar.getInstance(TimeZone.getDefault());
        TimePickerDialog timePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int h, int m) {
                endhour = h;
                endminute = m;
                cal.set(Calendar.HOUR_OF_DAY, endhour);
                cal.set(Calendar.MINUTE, endminute);
                ((TextView) findViewById(R.id.endTime)).setText(AddNewCourseActivity.TIME_FORMAT.format(cal.getTime()));
            }
        }, this.endhour, this.endminute, true);
        timePicker.setCancelable(false);
        timePicker.setTitle("Set End Time...");
        timePicker.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_new_course, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        ArrayList<Course> courses = null;
        courses = ((CourseManager) InternalStorage.readObject(getApplicationContext())).getCourses();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_new_event) {
            if (courses.isEmpty()) {
                Toast.makeText(getApplicationContext(), "You need a course to do that!", Toast.LENGTH_LONG).show();
            } else {
                Intent intent = new Intent(this, AddNewEventActivity.class);
                startActivity(intent);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void addAnotherCourseTime(View view){
        if(((CheckBox) findViewById(R.id.checkbox_Sunday)).isChecked()){
            ct.addTime(Day.SUNDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Monday)).isChecked()){
            ct.addTime(Day.MONDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Tuesday)).isChecked()){
            ct.addTime(Day.TUESDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Wednesday)).isChecked()){
            ct.addTime(Day.WEDNESDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Thursday)).isChecked()){
            ct.addTime(Day.THURSDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Friday)).isChecked()){
            ct.addTime(Day.FRIDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Saturday)).isChecked()){
            ct.addTime(Day.SATURDAY, starthour, startminute, endhour, endminute, null);
        }

        Toast.makeText(getApplicationContext(), "Class time saved!", Toast.LENGTH_LONG).show();

        ((CheckBox) findViewById(R.id.checkbox_Sunday)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkbox_Monday)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkbox_Tuesday)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkbox_Wednesday)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkbox_Thursday)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkbox_Friday)).setChecked(false);
        ((CheckBox) findViewById(R.id.checkbox_Saturday)).setChecked(false);

        rebuildList();
    }

    public void saveCourse(View view){
        //TODO: Fix crash on blank value entered.
        double homework,quizzes,projects,exams,finals;
        try
        {
            homework = Double.parseDouble(((EditText) findViewById(R.id.editText4)).getText().toString());
        }
        catch(NumberFormatException e)
        {
            homework = 0.0;
        }
        try
        {
            quizzes = Double.parseDouble(((EditText) findViewById(R.id.editText5)).getText().toString());
        }
        catch(NumberFormatException e)
        {
            quizzes = 0.0;
        }
        try
        {
            projects = Double.parseDouble(((EditText) findViewById(R.id.editText6)).getText().toString());
        }
        catch(NumberFormatException e)
        {
            projects = 0.0;
        }
        try
        {
            exams = Double.parseDouble(((EditText) findViewById(R.id.editText7)).getText().toString());
        }
        catch(NumberFormatException e)
        {
            exams = 0.0;
        }
        try
        {
            finals = Double.parseDouble(((EditText) findViewById(R.id.editText8)).getText().toString());
        }
        catch (NumberFormatException e)
        {
            finals = 0.0;
        }

        if(homework + quizzes + projects + exams + finals != 100.0){
            showGradeSystemError();
            return;
        }

        String title = ((EditText) findViewById(R.id.new_course_title)).getText().toString();
        if(title.equals("") || title == null){
            title = "Untitled Course";
        }

        String notes = ((EditText) findViewById(R.id.new_course_notes)).getText().toString();
        String courseCode = ((EditText) findViewById(R.id.courseCode)).getText().toString();
        String professor = ((EditText) findViewById(R.id.professor)).getText().toString();
        String syllubus = ((EditText) findViewById(R.id.syllubus)).getText().toString();

        if(((CheckBox) findViewById(R.id.checkbox_Sunday)).isChecked()){
            ct.addTime(Day.SUNDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Monday)).isChecked()){
            ct.addTime(Day.MONDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Tuesday)).isChecked()){
            ct.addTime(Day.TUESDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Wednesday)).isChecked()){
            ct.addTime(Day.WEDNESDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Thursday)).isChecked()){
            ct.addTime(Day.THURSDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Friday)).isChecked()){
            ct.addTime(Day.FRIDAY, starthour, startminute, endhour, endminute, null);
        } if(((CheckBox) findViewById(R.id.checkbox_Saturday)).isChecked()){
            ct.addTime(Day.SATURDAY, starthour, startminute, endhour, endminute, null);
        }

        Calendar begin = Calendar.getInstance();
        begin.set(Calendar.MONTH, beginmonth);
        begin.set(Calendar.DAY_OF_MONTH, beginday);
        begin.set(Calendar.YEAR, beginyear);

        Calendar end = Calendar.getInstance();
        end.set(Calendar.MONTH, endmonth);
        end.set(Calendar.DAY_OF_MONTH, endday);
        end.set(Calendar.YEAR, endyear);


        GradeSystem gradeSystem = new GradeSystem();
        gradeSystem.setHomeworkWeight(homework);
        gradeSystem.setQuizWeight(quizzes);
        gradeSystem.setProjectWeight(projects);
        gradeSystem.setExamWeight(exams);
        gradeSystem.setFinalWeight(finals);

            CourseManager courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());

            courseManager.addNewCourse(title, courseCode, begin.getTime(), end.getTime(), ct, notes, professor,
                    null, syllubus, gradeSystem);


            Alarms.RemindCourse(this,courseManager.getCourse(courseManager.getCourses().size() - 1));
            InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);

            Toast.makeText(getApplicationContext(), "Class saved!", Toast.LENGTH_LONG).show();
            Intent returnIntent = new Intent();
            setResult(RESULT_OK,returnIntent);

        finish();
    }

    private void showRemovePopUp(CourseMeeting meeting) {
        final CourseMeeting meeting2 = meeting;
        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Alert");
        helpBuilder.setMessage("Delete selected class time?");
        helpBuilder.setPositiveButton("Delete",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ct.removeTime(meeting2);
                        rebuildList();
                    }
                });
        helpBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                    }
                });
        // Remember, create doesn't show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }

    public void showGradeSystemError() {
        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Alert");
        helpBuilder.setMessage("Grade system weights must add to 100.0%");
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder quitAlert = new AlertDialog.Builder(this);
        quitAlert.setTitle(getString(R.string.title_dialog_quit_without_saving));
        quitAlert.setMessage(R.string.description_dialog_quit_without_saving);

        quitAlert.setPositiveButton(getString(R.string.dialog_quit_affirmative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        quitAlert.setNegativeButton(getString(R.string.dialog_negative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { }
                });

        quitAlert.show();
    }

}
