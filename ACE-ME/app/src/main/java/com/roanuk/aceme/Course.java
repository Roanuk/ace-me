package com.roanuk.aceme;

import java.io.Serializable;
import java.util.Date;

/**
 * Class used to represent a student's course with associated assignments, grades, and etc.
 */
public class Course implements Editable , Serializable{
    private String courseTitle;
    private Date startDate;
    private Date endDate;
    private CourseTimes courseTimes;
    private String professor;
    private Priority priority;
    private String syllabusPath;
    private String courseCode;
    private String notes;
    private CourseAssignments assignments;
    private GradeSystem gradeSystem;
    private double gradeGoal = 0;

    public Course(){
        courseTitle = "";
        startDate = null;
        endDate = null;
        courseTimes = new CourseTimes();
        professor = "";
        priority = Priority.MEDIUM;
        syllabusPath = "";
        courseCode = "";
        notes = "";
        assignments = new CourseAssignments();
        gradeSystem = new GradeSystem();
    }

    /**
     * method used to add an assignment to the course
     * @param a the assignment to be added
     */
    public void addAssignment(Assignment a){
        assignments.add(a);
    }

    /**
     * method used to remove an assignment from a course
     * @param a the assignment to be removed
     */
    public boolean removeAssignment(Assignment a){
        if(assignments.remove(a)){
            return true;
        }
        return false;
    }

    //public void replaceAssignment(Assignment a, Assignment b){ assignments.replaceAssignment(a, b); }

    /**
     * returns the course as a string, the course code plus the course title
     * @return courseCode + courseTitle
     */
    @Override
    public String toString(){
        if(courseCode.equals("") || courseCode == null){
            return courseTitle;
        }
        else
            return courseCode + ": " + courseTitle;
    }

    /**
     * method used to edit a course
     */
    public void edit(){
        // need to implement this method
        // start edit activity
    }

    /* getter and setter methods */
    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public CourseTimes getCourseTimes() {
        return courseTimes;
    }

    public void setCourseTimes(CourseTimes courseTimes) {
        this.courseTimes = courseTimes;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public String getSyllabusPath() {
        return syllabusPath;
    }

    public void setSyllabusPath(String syllabusPath) {
        this.syllabusPath = syllabusPath;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public CourseAssignments getAssignments() {
        return assignments;
    }

    public void setAssignments(CourseAssignments allAssignments) { this.assignments = allAssignments; }

    public GradeSystem getGradeSystem() {
        return gradeSystem;
    }

    public void setGradeSystem(GradeSystem gradeSystem) {
        this.gradeSystem = gradeSystem;
    }

    public double getGradeGoal() {
        return gradeGoal;
    }

    public void setGradeGoal(double gradeGoal) {
        this.gradeGoal = gradeGoal;
    }
}
