package com.roanuk.aceme;

import java.util.Date;

/**
 * Factory class used to create different types of assignments,
 * modeled after the pizza factory example in class
 */
public class AssignmentFactory {

    /**
     * creates a new assignment of type "type".
     * if the type is invalid, a null pointer is returned.
     * @param type the type of assignment to be created
     * @return the created assignment
     */
    public static Assignment createAssignment(AssignmentType type) {
        Assignment assignment = null;
        if (type.equals(AssignmentType.HOMEWORK)) {
            assignment = new Homework();
        } else if (type.equals(AssignmentType.QUIZ)) {
            assignment = new Quiz();
        } else if (type.equals(AssignmentType.EXAM)) {
            assignment = new Exam();
        } else if (type.equals(AssignmentType.PROJECT)) {
            assignment = new Project();
        } else if (type.equals(AssignmentType.FINAL)) {
            assignment = new Final();
        }
        return assignment;
    }

    /**
     * creates a new assignment of type "type" with fields set to the parameters.
     * if the type is invalid, a null pointer is returned.
     * @param type type of assignment to be created
     * @param dueDate the date the assignment is due
     * @param priority the priority of the assignment
     * @param notes user's notes for the assignment
     * @param gradeWeight the weight of the assignment on your final average
     * @param grade the grade made on the assignment
     * @return the created assignment
     */
    public static Assignment createAssignment(AssignmentType type, String title, Course parent, Date dueDate, Priority priority, Recurrence recurrence, Date recurrenceEndDate, String notes, double gradeWeight, double grade) {
        Assignment assignment = AssignmentFactory.createAssignment(type);
        assignment.setTitle(title);
        assignment.setParentCourse(parent);
        assignment.setDueDate(dueDate);
        assignment.setPriority(priority);
        assignment.setRecurrence(recurrence);
        assignment.setRecurrenceEndDate(recurrenceEndDate);
        assignment.setNotes(notes);
        assignment.setGradeWeight(gradeWeight);
        assignment.setGrade(grade);
        return assignment;
    }
}