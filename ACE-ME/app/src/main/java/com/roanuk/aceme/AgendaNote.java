package com.roanuk.aceme;

import java.io.Serializable;

import java.util.Date;

public class AgendaNote implements Serializable {

    private String notes;
    private Date date;

    public AgendaNote(String n, Date d){
        notes = n;
        date = d;
    }

    public String getNotes(){
        return notes;
    }

    public void setNotes(String n){
        notes = n;
    }

    public Date getDate(){
        return date;
    }
}
