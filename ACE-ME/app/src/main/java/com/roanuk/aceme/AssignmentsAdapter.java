package com.roanuk.aceme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class AssignmentsAdapter extends ArrayAdapter<Assignment> implements Serializable {
        public AssignmentsAdapter(Context context, ArrayList<Assignment> assignment) {
            super(context, 0, assignment);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            Assignment assignment = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.upcoming_list_item_xl, parent, false);
            }
            // Lookup view for data population
            TextView tvCourse = (TextView) convertView.findViewById(R.id.textView56);
            TextView tvType = (TextView) convertView.findViewById(R.id.textView57);
            // Populate the data into the template view using the data object
            tvCourse.setText(assignment.getTitle());
            Date date = assignment.getDueDate();
            String time = date.toString();
            time = time.replace(" CDT", "");
            int index = time.lastIndexOf(":");
            String time1 = time.substring(0,index - 6);
            tvType.setText(time1);
            // Return the completed view to render on screen
            return convertView;
        }
}