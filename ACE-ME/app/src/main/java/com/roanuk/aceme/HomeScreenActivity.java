package com.roanuk.aceme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.Calendar;

/**
 * This is the main activity for the android application ACE.ME
 */
public class HomeScreenActivity extends Activity {

    public static final boolean TESTING = false;
    private static CourseManager courseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        courseManager = (CourseManager) InternalStorage.setupInternalMemory(getApplicationContext());
        NoteManager note = (NoteManager) InternalStorage.setupInternalNote(getApplicationContext());

        // if testing, create fake courses and assignments
        if(TESTING){ makeTestCourses(); }
    }

    /* method called when the agenda button is pressed */
    public void agendaClickEvent(View view)
    {
        Intent intent = new Intent(this, AgendaActivity.class);
        startActivity(intent);
    }

    /* method called when the assignments button is pressed */
    public void assignmentsClickEvent(View view)
    {
        Intent intent = new Intent(this, AssignmentViewActivity.class);
        startActivity(intent);
    }

    /* method called when the tests button is pressed */
    public void testsClickEvent(View view)
    {
        Intent intent = new Intent(this, TestViewActivity.class);
        startActivity(intent);
    }

    /* method called when the courses button is pressed */
    public void coursesClickEvent(View view){
        Intent intent = new Intent(this, CourseViewActivity.class);
        startActivity(intent);
    }

    /* method called when the grades button is pressed */
    public void gradeClickEvent(View view)
    {
        //view.setBackgroundColor(getResources().getColor(android.R.color.holo_purple));
        Intent intent = new Intent(this, GradeViewActivity.class);
        startActivity(intent);
    }

    /* method called when the settings button is pressed */
    public void settingsClickEvent(View view)
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    /* method called when the plus button is pressed */
    public void newEventClickEvent(View view)
    {
        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        if(courseManager.getCourses().isEmpty()) {
            Intent intent = new Intent(this, AddNewCourseActivity.class);
            startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(this, AddNewEventActivity.class);
            startActivity(intent);
        }
    }

    /* method used to create a bunch of fake courses and assignments for testing purposes */
    public void makeTestCourses(){
        for(int i = 0; i < 4; i++) {
            courseManager.addNewCourse();
            Course temp = courseManager.getCourse(i);
            temp.setSyllabusPath("http://docs.google.com");
            temp.setPriority(Priority.MEDIUM);
            temp.setProfessor("Dr. Timothy Howard");
            temp.setCourseCode("EE 461L");
            CourseTimes ct = new CourseTimes();
            ct.addTime(Day.MONDAY, 9, 0, 10, 30, "ETC 2.102");
            ct.addTime(Day.WEDNESDAY, 9, 0, 10, 30, "ETC 2.102");
            ct.addTime(Day.TUESDAY, 18, 30, 21, 30, "ECJ 1.220");
            temp.setCourseTimes(ct);
            temp.setCourseTitle("Software Design Lab");
            temp.setStartDate(Calendar.getInstance().getTime());
            temp.setEndDate(Calendar.getInstance().getTime());
            temp.setNotes("These are my notes for the course.");
            GradeSystem gs = new GradeSystem();
            gs.setGradeSystem(20, 20, 20, 20, 20);
            temp.setGradeSystem(gs);
        }
        for(int i = 0; i < 4; i++){
            CourseAssignments ca = new CourseAssignments();
            for(int j = 0; j < 4; j++) {
                Assignment a = AssignmentFactory.createAssignment(AssignmentType.HOMEWORK, "HW " + j, courseManager.getCourse(i),
                        Calendar.getInstance().getTime(), Priority.MEDIUM, Recurrence.NONE, null, "These are my notes for the homework", 0, 87);
                ca.add(a);
            }
            for(int j = 0; j < 4; j++) {
                Assignment a = AssignmentFactory.createAssignment(AssignmentType.QUIZ, "Quiz " + j, courseManager.getCourse(i),
                        Calendar.getInstance().getTime(), Priority.MEDIUM, Recurrence.NONE, null, "These are my notes for the quiz", 0, 92);
                ca.add(a);
            }
            for(int j = 0; j < 4; j++) {
                Assignment a = AssignmentFactory.createAssignment(AssignmentType.PROJECT, "Project " + j, courseManager.getCourse(i),
                        Calendar.getInstance().getTime(), Priority.MEDIUM, Recurrence.NONE, null, "These are my notes for the project", 0, 100);
                ca.add(a);
            }
            for(int j = 0; j < 4; j++) {
                Assignment a = AssignmentFactory.createAssignment(AssignmentType.EXAM, "Exam " + j, courseManager.getCourse(i),
                        Calendar.getInstance().getTime(), Priority.MEDIUM, Recurrence.NONE, null, "These are my notes for the exam", 0, 95);
                ca.add(a);
            }
            for(int j = 0; j < 1; j++) {
                Assignment a = AssignmentFactory.createAssignment(AssignmentType.FINAL, "Final " + j, courseManager.getCourse(i),
                        Calendar.getInstance().getTime(), Priority.MEDIUM, Recurrence.NONE, null, "These are my notes for the final", 0, 97);
                ca.add(a);
            }
            courseManager.getCourse(i).setAssignments(ca);
        }
    }
}
