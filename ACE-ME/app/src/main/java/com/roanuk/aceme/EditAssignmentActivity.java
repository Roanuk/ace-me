package com.roanuk.aceme;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class EditAssignmentActivity extends ActionBarActivity implements
        DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener {

    static CourseManager courseManager;
    private static Course TEMP_COURSE;
    private static final SimpleDateFormat DATE_FORMAT =
            new SimpleDateFormat("MM/dd/yyyy", Locale.US);
    private static final int CREATE_COURSE_CALLBACK_MENU = 87;
    private static final int CREATE_COURSE_CALLBACK_ACTION_BAR = 89;
    private static final String NO_TITLE = "Untitled Assignment";
    private static final String TAG_DUE_DATE = "duedate";
    private static final String TAG_RECURRENCE = "recurrence";
    private static final String TAG_COURSES = "courseslist";
    private static final String TAG_DUE_DATE_SPINNER = "duedatespinner";
    private ArrayAdapter<CharSequence> eventAdapter;
    private Calendar cal;
    private int day;
    private int month;
    private int year;
    private Calendar recurrenceCal;
    private int recurDay;
    private int recurMonth;
    private int recurYear;
    static Assignment currentAssignment;
    private Course parentCourse;
    private Date dueDate;
    private double grade;
    private double gradeWeight;
    private String notes;
    private String title;
    private Priority priority;
    private Recurrence recurrence;
    private Date recurrenceEndDate;
    private AssignmentType type;
    private int dueDatePref;
    private boolean dateMutated = false;
    private int parentIndex = 0;

    static {
        TEMP_COURSE = new Course();
        TEMP_COURSE.setCourseTitle("Add New Course...");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_assignment);

        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        currentAssignment = courseManager.getCurrentAssignment();
        parentCourse = currentAssignment.getParentCourse();
        parentIndex = courseManager.getCourses().indexOf(parentCourse);
        dueDate = currentAssignment.getDueDate();
        grade = currentAssignment.getGrade();
        gradeWeight = currentAssignment.getGradeWeight();
        notes = currentAssignment.getNotes();
        priority = currentAssignment.getPriority();
        recurrence = currentAssignment.getRecurrence();
        recurrenceEndDate = currentAssignment.getRecurrenceEndDate();
        recurrenceCal = Calendar.getInstance();
        recurrenceCal.setTime(recurrenceEndDate);
        recurDay = recurrenceCal.get(Calendar.DAY_OF_MONTH);
        recurMonth = recurrenceCal.get(Calendar.MONTH);
        recurYear = recurrenceCal.get(Calendar.YEAR);
        title = currentAssignment.getTitle();
        type = currentAssignment.instanceOf();
        cal = Calendar.getInstance(TimeZone.getDefault());
        cal.setTime(dueDate);
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        dueDatePref = currentAssignment.getDueDatePref();
        if(dueDatePref == 3) { dateMutated = true; }

        Spinner eventSpinner = (Spinner) findViewById(R.id.new_event_type_spinner);
        eventAdapter = ArrayAdapter.createFromResource(this,
                R.array.event_types_array, android.R.layout.simple_spinner_dropdown_item);
        eventAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        eventSpinner.setAdapter(eventAdapter);
        eventSpinner.setClickable(true);
        eventSpinner.setEnabled(true);

        if(type.equals(AssignmentType.HOMEWORK)){
            eventSpinner.setSelection(0);
        } else if(type.equals(AssignmentType.QUIZ)){
            eventSpinner.setSelection(1);
        } else if(type.equals(AssignmentType.EXAM)){
            eventSpinner.setSelection(2);
        } else if(type.equals(AssignmentType.PROJECT)){
            eventSpinner.setSelection(3);
        } else if(type.equals(AssignmentType.FINAL)){
            eventSpinner.setSelection(4);
        }

        ((EditText)findViewById(R.id.new_event_title)).setText(title);

        ((TextView)findViewById(R.id.date_picker_edit)).setText(EditAssignmentActivity.DATE_FORMAT.format(dueDate));
        ((TextView)findViewById(R.id.end_recurring_date_picker_edit))
                .setText(EditAssignmentActivity.DATE_FORMAT.format(recurrenceEndDate));

        rebuildCourseList();

        Spinner dueDateSpinner = (Spinner) findViewById(R.id.new_event_due_date_spinner_edit);
        ArrayAdapter<CharSequence> dueDateAdapter = ArrayAdapter.createFromResource(this,
                R.array.pref_list_defaultDueDate, android.R.layout.simple_spinner_dropdown_item);
        dueDateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dueDateSpinner.setAdapter(dueDateAdapter);
        dueDateSpinner.setTag(TAG_DUE_DATE_SPINNER);
        dueDateSpinner.setOnItemSelectedListener(this);

        dueDateSpinner.setSelection(dueDatePref);
        View datePicker = findViewById(R.id.date_picker_edit);
        switch(dueDatePref) {
            case 0:
            case 1:
            case 2:
                dateMutated = false;
                datePicker.setClickable(false);
                break;
            case 3:
                dateMutated = true;
                datePicker.setClickable(true);
        }

        Spinner prioritySpinner = (Spinner) findViewById(R.id.new_event_priority_spinner);
        ArrayAdapter<Priority> priorityAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, Priority.values());
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prioritySpinner.setAdapter(priorityAdapter);

        if(priority.equals(Priority.HIGHEST)){
            prioritySpinner.setSelection(0);
        } else if(priority.equals(Priority.HIGH)){
            prioritySpinner.setSelection(1);
        } else if(priority.equals(Priority.MEDIUM)){
            prioritySpinner.setSelection(2);
        } else if(priority.equals(Priority.LOW)){
            prioritySpinner.setSelection(3);
        } else if(priority.equals(Priority.LOWEST)){
            prioritySpinner.setSelection(4);
        } else if(priority.equals(Priority.NONE)){
            prioritySpinner.setSelection(5);
        }

        Spinner recurrenceSpinner = (Spinner) findViewById(R.id.new_event_recurring_spinner);
        ArrayAdapter<Recurrence> recurrenceAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, Recurrence.values());
        recurrenceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        recurrenceSpinner.setAdapter(recurrenceAdapter);

        if(recurrence.equals(Recurrence.NONE)){
            recurrenceSpinner.setSelection(0);
        } else if(recurrence.equals(Recurrence.EVERY_DAY)){
            recurrenceSpinner.setSelection(1);
        } else if(recurrence.equals(Recurrence.EVERY_CLASS)){
            recurrenceSpinner.setSelection(2);
        } else if(recurrence.equals(Recurrence.EVERY_WEEK)){
            recurrenceSpinner.setSelection(3);
        } else if(recurrence.equals(Recurrence.EVERY_TWO_WEEKS)){
            recurrenceSpinner.setSelection(4);
        }

        ((EditText)findViewById(R.id.new_event_notes)).setText(notes);
        ((EditText)findViewById(R.id.new_event_grade_value)).setText("" + grade);
        ((EditText)findViewById(R.id.new_event_grade_weight)).setText("" + gradeWeight);
    }

    private void rebuildCourseList() {
        Spinner spinner = (Spinner) findViewById((R.id.new_event_parent_course_spinner));
        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        currentAssignment = courseManager.getCurrentAssignment();
        parentCourse = currentAssignment.getParentCourse();
        parentIndex = courseManager.getCourses().indexOf(parentCourse);
        List<Course> coursesCopy = new ArrayList<>(courseManager.getCourses());
        coursesCopy.add(TEMP_COURSE);
        ArrayAdapter<Course> courseAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, coursesCopy);
        courseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(courseAdapter);
        spinner.setTag(TAG_COURSES);
        spinner.setOnItemSelectedListener(this);

        int index = coursesCopy.indexOf(parentCourse);
        spinner.setSelection(index);
    }

    public void setDateEdit(View view) {
        DatePickerDialog datePicker =
                new DatePickerDialog(this, this, this.year, this.month, this.day);
        datePicker.setCancelable(false);
        datePicker.setTitle(R.string.description_add_event_set_date);
        datePicker.getDatePicker().setTag(TAG_DUE_DATE);
        datePicker.show();
    }

    public void setRecurringEndDateEdit(View view) {
        DatePickerDialog datePicker =
                new DatePickerDialog(this, this, this.recurYear, this.recurMonth, this.recurDay);
        datePicker.setCancelable(false);
        datePicker.setTitle(R.string.description_add_event_set_date);
        datePicker.getDatePicker().setTag(TAG_RECURRENCE);
        datePicker.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String spinnerTag = parent.getTag().toString();
        if(spinnerTag != null) {
            if(spinnerTag == TAG_COURSES) {
                if(position == parent.getCount() - 1) {
                    Intent intent = new Intent(view.getContext(), AddNewCourseActivity.class);
                    startActivityForResult(intent, CREATE_COURSE_CALLBACK_MENU);
                } else if(!dateMutated) {
                    regenerateNextCourseDate();
                }
            } else if(spinnerTag == TAG_DUE_DATE_SPINNER) {
                View datePicker = findViewById(R.id.date_picker_edit);
                switch(position) {
                    case 0:
                        dateMutated = false;
                        datePicker.setClickable(false);
                        datePicker.setEnabled(false);
                        regenerateNextCourseDate();
                        break;
                    case 1:
                        dateMutated = false;
                        datePicker.setClickable(false);
                        datePicker.setEnabled(false);
                        cal = Calendar.getInstance(TimeZone.getDefault());
                        cal.add(Calendar.DAY_OF_MONTH, 7);
                        break;
                    case 2:
                        dateMutated = false;
                        datePicker.setClickable(false);
                        datePicker.setEnabled(false);
                        cal = Calendar.getInstance(TimeZone.getDefault());
                        cal.add(Calendar.DAY_OF_MONTH, 14);
                        break;
                    case 3:
                        dateMutated = true;
                        datePicker.setClickable(true);
                        datePicker.setEnabled(true);
                }

                day = cal.get(Calendar.DAY_OF_MONTH);
                month = cal.get(Calendar.MONTH);
                year = cal.get(Calendar.YEAR);

                ((TextView) findViewById(R.id.date_picker_edit))
                        .setText(DATE_FORMAT.format(cal.getTime()));
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    private void regenerateNextCourseDate() {
        Spinner courseSpinner =
                (Spinner) findViewById((R.id.new_event_parent_course_spinner));
        Course newParentCourse = (Course) courseSpinner.getSelectedItem();
        ArrayList<CourseMeeting> meetings =
                newParentCourse.getCourseTimes().getSortedMeetings();
        if(!meetings.isEmpty()) {
            CourseMeeting earliestMeeting = meetings.get(0);
            Calendar now = Calendar.getInstance(TimeZone.getDefault());
            for (CourseMeeting c : meetings) {
                if(c.getCalendarDay() > now.get(Calendar.DAY_OF_WEEK) &&
                        earliestMeeting.compareTo(c) > 0) {
                    earliestMeeting = c;
                }
            }
            while(cal.get(Calendar.DAY_OF_WEEK) != earliestMeeting.getCalendarDay()) {
                cal.add(Calendar.DAY_OF_MONTH, 1);
            }
            cal.set(Calendar.HOUR_OF_DAY, earliestMeeting.getStartHour());
            cal.set(Calendar.MINUTE, earliestMeeting.getStartMinute());

        } else { cal = Calendar.getInstance(TimeZone.getDefault()); }

        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        ((TextView)findViewById(R.id.date_picker_edit)).setText(DATE_FORMAT.format(cal.getTime()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Spinner spinner = (Spinner) findViewById(R.id.new_event_parent_course_spinner);
        switch(requestCode) {
            case CREATE_COURSE_CALLBACK_MENU:
            case CREATE_COURSE_CALLBACK_ACTION_BAR:
                switch(resultCode) {
                    case Activity.RESULT_OK:
                        rebuildCourseList();
                        spinner.setSelection(spinner.getCount() - 2);
                        break;
                    case Activity.RESULT_CANCELED:
                    default:
                        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
                        currentAssignment = courseManager.getCurrentAssignment();
                        parentCourse = currentAssignment.getParentCourse();
                        parentIndex = courseManager.getCourses().indexOf(parentCourse);
                        List<Course> coursesCopy = new ArrayList<>(courseManager.getCourses());
                        int index = coursesCopy.indexOf(parentCourse);
                        spinner.setSelection(index);
                }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        String dateTag = view.getTag().toString();
        if(dateTag != null) {
            if (dateTag == TAG_DUE_DATE) {
                this.day = day;
                this.month = month;
                this.year = year;
                cal.set(Calendar.DAY_OF_MONTH, day);
                cal.set(Calendar.MONTH, month);
                cal.set(Calendar.YEAR, year);
                ((TextView) findViewById(R.id.date_picker_edit)).setText(DATE_FORMAT.format(cal.getTime()));
            } else if (dateTag == TAG_RECURRENCE) {
                this.recurDay = day;
                this.recurMonth = month;
                this.recurYear = year;
                recurrenceCal = Calendar.getInstance();
                recurrenceCal.set(Calendar.DAY_OF_MONTH, day);
                recurrenceCal.set(Calendar.MONTH, month);
                recurrenceCal.set(Calendar.YEAR, year);
                ((TextView) findViewById(R.id.end_recurring_date_picker_edit)).setText(DATE_FORMAT.format(recurrenceCal.getTime()));
            }
        }
    }

    public void remove(View v) {
        AlertDialog.Builder deleteAlert = new AlertDialog.Builder(this);
        deleteAlert.setTitle(getString(R.string.title_dialog_delete_assignment));
        deleteAlert.setMessage(R.string.description_dialog_delete_assignment);

        deleteAlert.setPositiveButton(getString(R.string.dialog_delete_affirmative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Recurrence r = currentAssignment.getRecurrence();
                        cal.setTime(dueDate);
                        if (r.equals(Recurrence.EVERY_DAY)) {
                            cal.add(Calendar.DAY_OF_MONTH, 1);
                            dueDate = cal.getTime();
                        } else if (r.equals(Recurrence.EVERY_CLASS)) {
                            dueDate = regenerateNextCourseDate(currentAssignment.getDueDate());
                        } else if (r.equals(Recurrence.EVERY_WEEK)) {
                            cal.add(Calendar.DAY_OF_MONTH, 7);
                            dueDate = cal.getTime();
                        } else if (r.equals(Recurrence.EVERY_TWO_WEEKS)) {
                            cal.add(Calendar.DAY_OF_MONTH, 14);
                            dueDate = cal.getTime();
                        }

                        if (!(currentAssignment.getRecurrence().equals(Recurrence.NONE)) &&
                                (currentAssignment.getRecurrenceEndDate().after(dueDate))) {

                            Assignment assignment = AssignmentFactory.createAssignment(type, title,
                                    parentCourse, dueDate, priority, recurrence, recurrenceEndDate,
                                    notes, gradeWeight, grade);

                            parentCourse.addAssignment(assignment);
                            Alarms.RemindAssignment(EditAssignmentActivity.this, assignment);
                        }
                        //int index = courseManager.getCourses().indexOf(parentCourse);
                        parentCourse = courseManager.getCourse(parentIndex);

                        parentCourse.removeAssignment(currentAssignment);

                        InternalStorage.refreshInternalMemory(
                                getApplicationContext(), courseManager);

                        Toast.makeText(getApplicationContext(), "Assignment Removed!",
                                Toast.LENGTH_LONG).show();
                        finish();
                    }
                });
        deleteAlert.setNegativeButton(getString(R.string.dialog_negative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        deleteAlert.show();
    }

    private Date regenerateNextCourseDate(Date currentDueDate) {
        Spinner courseSpinner =
                (Spinner) findViewById((R.id.new_event_parent_course_spinner));
        Course newParentCourse = (Course) courseSpinner.getSelectedItem();
        ArrayList<CourseMeeting> meetings =
                newParentCourse.getCourseTimes().getSortedMeetings();
        if(!meetings.isEmpty()) {
            CourseMeeting earliestMeeting = meetings.get(0);
            Calendar now = Calendar.getInstance();
            now.setTime(currentDueDate);
            for (CourseMeeting c : meetings) {
                if(c.getCalendarDay() > now.get(Calendar.DAY_OF_WEEK) &&
                        earliestMeeting.compareTo(c) > 0) {
                    earliestMeeting = c;
                }
            }
            now.add(Calendar.DAY_OF_MONTH, 1);
            while(now.get(Calendar.DAY_OF_WEEK) != earliestMeeting.getCalendarDay()) {
                now.add(Calendar.DAY_OF_MONTH, 1);
            }
            while(now.get(Calendar.HOUR_OF_DAY) != earliestMeeting.getStartHour()) {
                now.add(Calendar.HOUR_OF_DAY, 1);
            }
            while(now.get(Calendar.MINUTE) != earliestMeeting.getStartMinute()) {
                now.add(Calendar.MINUTE, 1);
            }

            return now.getTime();
        }
        Toast.makeText(getApplicationContext(), "null returned",
                Toast.LENGTH_LONG).show();
        return currentAssignment.getDueDate();
    }

    public void onSubmitEdit(View v) {
        AssignmentType assignmentType = null;
        String type = ((Spinner) findViewById(R.id.new_event_type_spinner)).getSelectedItem().toString();
        if(type.equals("Homework")){
            assignmentType = AssignmentType.HOMEWORK;
        } else if(type.equals("Quiz")){
            assignmentType = AssignmentType.QUIZ;
        } else if(type.equals("Project")){
            assignmentType = AssignmentType.PROJECT;
        } else if(type.equals("Exam")){
            assignmentType = AssignmentType.EXAM;
        } else if(type.equals("Final")){
            assignmentType = AssignmentType.FINAL;
        }

        String title =  ((EditText)findViewById(R.id.new_event_title)).getText().toString();
        if(title.equals("") || title == null){
            title = NO_TITLE;
        }

        Date dueDate = cal.getTime();
        Date recurrenceEndDate = recurrenceCal.getTime();

        Spinner courseSpinner = (Spinner)findViewById((R.id.new_event_parent_course_spinner));
        Course newParentCourse = (Course) courseSpinner.getSelectedItem();

        Spinner dueDateSpinner = (Spinner) findViewById(R.id.new_event_due_date_spinner_edit);
        int dueDatePref = dueDateSpinner.getSelectedItemPosition();

        Spinner prioritySpinner = (Spinner) findViewById(R.id.new_event_priority_spinner);
        Priority priority = (Priority) prioritySpinner.getSelectedItem();

        Spinner recurrenceSpinner = (Spinner) findViewById(R.id.new_event_recurring_spinner);
        Recurrence recurrence = (Recurrence) recurrenceSpinner.getSelectedItem();

        String notes = ((EditText)findViewById(R.id.new_event_notes)).getText().toString();

        String gradeWeight = ((EditText) findViewById(R.id.new_event_grade_weight)).getText().toString();
        if(gradeWeight.equals("") || gradeWeight == null){
            gradeWeight = "0";
        }

        String gradeValue = ((EditText) findViewById(R.id.new_event_grade_value)).getText().toString();
        if(gradeValue.equals("") || gradeValue == null) {
            gradeValue = "0";
        }

        Assignment assignment = AssignmentFactory.createAssignment(assignmentType, title, newParentCourse,
                dueDate, priority, recurrence, recurrenceEndDate, notes, Double.parseDouble(gradeWeight), Double.parseDouble(gradeValue));

        assignment.setDueDatePref(dueDatePref);

        //int index = courseManager.getCourses().indexOf(parentCourse);
        int index2 = courseManager.getCourses().indexOf(newParentCourse);
        parentCourse = courseManager.getCourse(parentIndex);
        newParentCourse = courseManager.getCourse(index2);

        parentCourse.removeAssignment(currentAssignment);
        newParentCourse.addAssignment(assignment);
        Alarms.RemindAssignment(this, assignment);
        InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);

        Toast.makeText(getBaseContext(), R.string.add_new_event_saved, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder quitAlert = new AlertDialog.Builder(this);
        quitAlert.setTitle(getString(R.string.title_dialog_quit_without_saving));
        quitAlert.setMessage(R.string.description_dialog_quit_without_saving);

        quitAlert.setPositiveButton(getString(R.string.dialog_quit_affirmative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        quitAlert.setNegativeButton(getString(R.string.dialog_negative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { }
                });

        quitAlert.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add_course) {
            Intent intent = new Intent(this, AddNewCourseActivity.class);
            startActivityForResult(intent, CREATE_COURSE_CALLBACK_ACTION_BAR);
        }

        return super.onOptionsItemSelected(item);
    }

    public static void setCurrentAssignment(Assignment a){
        currentAssignment = a;
    }
}
