package com.roanuk.aceme;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Assignment class, represents a course assignment
 */
public class Assignment implements Editable, Comparable<Assignment>, Serializable
{
    private String Title;
    private Course parentCourse;
    private Date dueDate = new Date();
    private Priority priority;
    private Recurrence recurrence;
    private String notes;
    private double gradeWeight;
    private double grade;
    private Date recurrenceEndDate;
    private int dueDatePref;

    /**
     * Method used to get the type of assignment.
     * If the assignment is not a valid type of assignment, return null.
     * @return the AssignmentType enum representing the type of assignment
     */
    public AssignmentType instanceOf() {
        if(this instanceof Homework)
            return AssignmentType.HOMEWORK;
        else if(this instanceof Quiz)
            return AssignmentType.QUIZ;
        else if(this instanceof Project)
            return AssignmentType.PROJECT;
        else if(this instanceof Exam)
            return AssignmentType.EXAM;
        else if(this instanceof Final)
            return AssignmentType.FINAL;
        else
            return null;
    }

    @Override
    public int compareTo(Assignment that)
    {

        Calendar thisCal = Calendar.getInstance();
        Calendar thatCal = Calendar.getInstance();
        thisCal.setTime(this.getDueDate());
        thatCal.setTime(that.getDueDate());
        int thisDay = thisCal.get(Calendar.DAY_OF_MONTH);
        int thisMonth = thisCal.get(Calendar.MONTH);
        int thisYear = thisCal.get(Calendar.YEAR);
        int thatDay = thatCal.get(Calendar.DAY_OF_MONTH);
        int thatMonth = thatCal.get(Calendar.MONTH);
        int thatYear = thatCal.get(Calendar.YEAR);
        int x;

        if(thisYear > thatYear){
            x = 1;
        } else if(thisMonth > thatMonth && thisYear == thatYear){
            x = 1;
        } else if(thisDay > thatDay && thisMonth == thatMonth && thisYear == thatYear){
            x = 1;
        } else if(thisDay == thatDay && thisMonth == thatMonth && thisYear == thatYear){
            x = 0;
        } else{
            x = -1;
        }

        if(x == 0){
            Priority thisPri = this.getPriority();
            Priority thatPri = that.getPriority();
            int thisVal = thisPri.getVal();
            int thatVal = thatPri.getVal();

            if(thisVal > thatVal){
                x = -1;
            }
            else if(thisVal < thatVal){
                x = 1;
            }
        }

        return x;
    }

    public void edit(){}

    /**
     * method used to edit an assignment
     */
    public void edit(String title, Course course, Date dueDate, Priority priority, Recurrence recurrence, Date recurrenceEndDate, String notes,
                     double gradeWeight, double gradeValue){
        setTitle(title);
        setParentCourse(course);
        setDueDate(dueDate);
        setPriority(priority);
        setRecurrence(recurrence);
        setRecurrenceEndDate(recurrenceEndDate);
        setNotes(notes);
        setGradeWeight(gradeWeight);
        setGrade(gradeValue);
    }

    /* getter and setter methods for all the fields */
    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Course getParentCourse() {
        return parentCourse;
    }

    public void setParentCourse(Course parentCourse) {
        this.parentCourse = parentCourse;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public double getGradeWeight() {
        return gradeWeight;
    }

    public void setGradeWeight(double gradeWeight) {
        this.gradeWeight = gradeWeight;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public Recurrence getRecurrence() { return recurrence; }

    public void setRecurrence(Recurrence recurrence) { this.recurrence = recurrence; }

    public Date getRecurrenceEndDate() { return recurrenceEndDate; }

    public void setRecurrenceEndDate(Date recurrenceEndDate) { this.recurrenceEndDate = recurrenceEndDate; }

    public int getDueDatePref() {
        return dueDatePref;
    }

    public void setDueDatePref(int dueDatePref) {
        this.dueDatePref = dueDatePref;
    }
}
