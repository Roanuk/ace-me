package com.roanuk.aceme;

import java.io.Serializable;

/**
 * class that represents a grade system used for a course
 */
public class GradeSystem implements Serializable {

    private double homeworkWeight;
    private double projectWeight;
    private double quizWeight;
    private double examWeight;
    private double finalWeight;

    public void setGradeSystem(double hw, double proj, double quiz, double exam, double finalW){
        homeworkWeight = hw;
        projectWeight = proj;
        quizWeight = quiz;
        examWeight = exam;
        finalWeight = finalW;
    }

    public double getHomeworkWeight() {
        return homeworkWeight;
    }

    public void setHomeworkWeight(double homeworkWeight) {
        this.homeworkWeight = homeworkWeight;
    }

    public double getProjectWeight() {
        return projectWeight;
    }

    public void setProjectWeight(double projectWeight) {
        this.projectWeight = projectWeight;
    }

    public double getQuizWeight() {
        return quizWeight;
    }

    public void setQuizWeight(double quizWeight) {
        this.quizWeight = quizWeight;
    }

    public double getExamWeight() {
        return examWeight;
    }

    public void setExamWeight(double examWeight) {
        this.examWeight = examWeight;
    }

    public double getFinalWeight() {
        return finalWeight;
    }

    public void setFinalWeight(double finalWeight) {
        this.finalWeight = finalWeight;
    }
}
