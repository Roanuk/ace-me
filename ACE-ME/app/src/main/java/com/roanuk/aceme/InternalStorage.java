package com.roanuk.aceme;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.provider.ContactsContract;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public final class InternalStorage{

    private InternalStorage() {}

    public static void writeObject(Context context, Object object) {
        try {
            FileOutputStream fos = context.openFileOutput(CourseManager.KEY, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
        } catch(IOException e){
            showInternalMemoryError(context);
        }
    }

    public static void writeNote(Context context, Object object) {
        try {
            FileOutputStream fos = context.openFileOutput(NoteManager.KEY, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
        } catch(IOException e){
            showInternalMemoryError(context);
        }
    }

    public static Object readObject(Context context) {
        Object object = null;
        try {
            FileInputStream fis = context.openFileInput(CourseManager.KEY);
            ObjectInputStream ois = new ObjectInputStream(fis);
            object = ois.readObject();
            return object;
        } catch(IOException e){
            return setupInternalMemory(context);
        } catch(ClassNotFoundException e){
            return setupInternalMemory(context);
        }
    }

    public static Object readNote(Context context) {
        Object object = null;
        try {
            FileInputStream fis = context.openFileInput(NoteManager.KEY);
            ObjectInputStream ois = new ObjectInputStream(fis);
            object = ois.readObject();
            return object;
        } catch(IOException e){
            return setupInternalMemory(context);
        } catch(ClassNotFoundException e){
            return setupInternalMemory(context);
        }
    }

    public static void deleteObject(Context context, String key) {
        context.deleteFile(key);
       // if(Integer.parseInt(CourseManager.KEY) + 1 < 5000)
       // CourseManager.KEY = Integer.toString(Integer.parseInt(CourseManager.KEY) + 1);
    }

    public static void deleteNote(Context context, String key) {
        context.deleteFile(key);
    }

    public static void showInternalMemoryError(Context context) {
        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(context);
        helpBuilder.setTitle("ERROR");
        helpBuilder.setMessage("ERROR 461: Internal Memory Error. Please restart your app.");
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }

    public static void refreshInternalMemory(Context context, Object object){
        InternalStorage.deleteObject(context, CourseManager.KEY);
        InternalStorage.writeObject(context, object);
    }

    public static void refreshInternalNote(Context context, Object object){
        InternalStorage.deleteNote(context, NoteManager.KEY);
        InternalStorage.writeNote(context, object);
    }

    public static Object setupInternalNote(Context context){
        Object object = new NoteManager();
        try {
            FileInputStream fis = context.openFileInput(NoteManager.KEY);
            ObjectInputStream ois = new ObjectInputStream(fis);
            object = ois.readObject();
            return object;
        } catch(IOException e){
            try {
                FileOutputStream fos = context.openFileOutput(NoteManager.KEY, Context.MODE_PRIVATE);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                object = new NoteManager();
                oos.writeObject(object);
                oos.close();
                fos.close();
                return object;
            } catch(IOException ex){
                showInternalMemoryError(context);
            }
        } catch(ClassNotFoundException e){
            try {
                FileOutputStream fos = context.openFileOutput(NoteManager.KEY, Context.MODE_PRIVATE);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                object = new NoteManager();
                oos.writeObject(object);
                oos.close();
                fos.close();
                return object;
            } catch(IOException ex){
                showInternalMemoryError(context);
            }
        }
        return object;
    }

    public static Object setupInternalMemory(Context context){
        Object object = new CourseManager();
        try {
            FileInputStream fis = context.openFileInput(CourseManager.KEY);
            ObjectInputStream ois = new ObjectInputStream(fis);
            object = ois.readObject();
            return object;
        } catch(IOException e){
            try {
                FileOutputStream fos = context.openFileOutput(CourseManager.KEY, Context.MODE_PRIVATE);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                object = new CourseManager();
                oos.writeObject(object);
                oos.close();
                fos.close();
                return object;
            } catch(IOException ex){
                showInternalMemoryError(context);
            }
        } catch(ClassNotFoundException e){
            try {
                FileOutputStream fos = context.openFileOutput(CourseManager.KEY, Context.MODE_PRIVATE);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                object = new CourseManager();
                oos.writeObject(object);
                oos.close();
                fos.close();
                return object;
            } catch(IOException ex){
                showInternalMemoryError(context);
            }
        }
        return object;
    }
}