package com.roanuk.aceme;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class AddNewEventActivity extends ActionBarActivity implements
        DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener {

    private CourseManager courseManager;
    private static Course TEMP_COURSE;
    private static final SimpleDateFormat DATE_FORMAT =
            new SimpleDateFormat("MM/dd/yyyy", Locale.US);
    private static final int CREATE_COURSE_CALLBACK_MENU = 87;
    private static final int CREATE_COURSE_CALLBACK_ACTION_BAR = 89;
    private static final String TAG_DUE_DATE = "duedate";
    private static final String TAG_RECURRENCE = "recurrence";
    private static final String TAG_COURSES = "courseslist";
    private static final String TAG_TITLES = "titleformatlist";
    private static final String TAG_DUE_DATE_SPINNER = "duedatespinner";
    private static final String SEPARATOR = " ";
    private static final String PREFERENCE_DEFAULT_TITLE = "pref_default_event_title_list";
    private static final String PREFERENCE_DEFAULT_TYPE = "pref_default_event_type_list";
    private static final String PREFERENCE_DEFAULT_PRIORITY = "pref_default_priority_list";
    private static final String PREFERENCE_DEFAULT_RECURRENCE = "pref_default_recurrence_list";
    private static final String PREFERENCE_DEFAULT_DUE_DATE = "pref_default_dueDate_list";
    private Calendar cal;
    private int day;
    private int month;
    private int year;
    private Calendar recurrenceCal;
    private int recurDay;
    private int recurMonth;
    private int recurYear;
    private boolean dateMutated = true;

    static {
        TEMP_COURSE = new Course();
        TEMP_COURSE.setCourseTitle("Add a new course...");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_event);

        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());

        Spinner eventSpinner = (Spinner) findViewById(R.id.new_event_type_spinner);
        ArrayAdapter<CharSequence> eventAdapter = ArrayAdapter.createFromResource(this,
                R.array.event_types_array, android.R.layout.simple_spinner_dropdown_item);
        eventAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        eventSpinner.setAdapter(eventAdapter);
        eventSpinner.setClickable(true);
        eventSpinner.setEnabled(true);

        Spinner titleSpinner = (Spinner) findViewById(R.id.new_event_title_type_spinner);
        ArrayAdapter<CharSequence> titleAdapter = ArrayAdapter.createFromResource(this,
                R.array.pref_list_defaultTitles, android.R.layout.simple_spinner_dropdown_item);
        titleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        titleSpinner.setAdapter(titleAdapter);
        titleSpinner.setClickable(true);
        titleSpinner.setEnabled(true);
        titleSpinner.setTag(TAG_TITLES);
        titleSpinner.setOnItemSelectedListener(this);

        recurrenceCal = Calendar.getInstance(TimeZone.getDefault());
        recurDay = recurrenceCal.get(Calendar.DAY_OF_MONTH);
        recurMonth = recurrenceCal.get(Calendar.MONTH);
        recurYear = recurrenceCal.get(Calendar.YEAR);

        ((TextView)findViewById(R.id.end_recurring_date_picker))
                .setText(DATE_FORMAT.format(recurrenceCal.getTime()));

        rebuildCourseList();

        Spinner dueDateSpinner = (Spinner) findViewById(R.id.new_event_due_date_spinner);
        ArrayAdapter<CharSequence> dueDateAdapter = ArrayAdapter.createFromResource(this,
                R.array.pref_list_defaultDueDate, android.R.layout.simple_spinner_dropdown_item);
        dueDateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dueDateSpinner.setAdapter(dueDateAdapter);
        dueDateSpinner.setTag(TAG_DUE_DATE_SPINNER);
        dueDateSpinner.setOnItemSelectedListener(this);

        Spinner prioritySpinner = (Spinner) findViewById(R.id.new_event_priority_spinner);
        ArrayAdapter<Priority> priorityAdapter =
                new ArrayAdapter<>(
                        this, android.R.layout.simple_spinner_dropdown_item, Priority.values());
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prioritySpinner.setAdapter(priorityAdapter);

        ((EditText) findViewById(R.id.new_event_grade_weight)).setText("1.0");

        Spinner recurrenceSpinner = (Spinner) findViewById(R.id.new_event_recurring_spinner);
        ArrayAdapter<Recurrence> recurrenceAdapter =
                new ArrayAdapter<>(
                        this, android.R.layout.simple_spinner_dropdown_item, Recurrence.values());
        recurrenceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        recurrenceSpinner.setAdapter(recurrenceAdapter);

        SharedPreferences eventPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        titleSpinner.setSelection(
                Integer.valueOf(eventPrefs.getString(PREFERENCE_DEFAULT_TITLE, "3")));

        if(!getIntent().getBooleanExtra("isTest",false)) {
            eventSpinner.setSelection(
                    Integer.valueOf(eventPrefs.getString(PREFERENCE_DEFAULT_TYPE, "0")));
        }
        else
        {
            eventSpinner.setSelection(2); //2 is exam, 4 is final
        }

        prioritySpinner.setSelection(
                Integer.valueOf(eventPrefs.getString(PREFERENCE_DEFAULT_PRIORITY, "5")));

        recurrenceSpinner.setSelection(
                Integer.valueOf(eventPrefs.getString(PREFERENCE_DEFAULT_RECURRENCE, "0")));

        cal = Calendar.getInstance(TimeZone.getDefault());

        int dueDatePref = Integer.valueOf(eventPrefs.getString(PREFERENCE_DEFAULT_DUE_DATE, "0"));
        dueDateSpinner.setSelection(dueDatePref);
        View datePicker = findViewById(R.id.date_picker);
        switch(dueDatePref) {
            case 0:
                dateMutated = false;
                datePicker.setClickable(false);
                break;
            case 1:
                cal.add(Calendar.DAY_OF_MONTH, 7);
                datePicker.setClickable(false);
                break;
            case 2:
                cal.add(Calendar.DAY_OF_MONTH, 14);
                datePicker.setClickable(false);
                break;
            case 3:
                dateMutated = true;
                datePicker.setClickable(true);
        }

        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);

        ((TextView)findViewById(R.id.date_picker)).setText(DATE_FORMAT.format(cal.getTime()));

        recurrenceCal = Calendar.getInstance(TimeZone.getDefault());
        recurDay = day;
        recurMonth = month;
        recurYear = year;
        recurrenceCal.set(Calendar.DAY_OF_MONTH, day);
        recurrenceCal.set(Calendar.MONTH, month);
        recurrenceCal.set(Calendar.YEAR, year);

        ((TextView)findViewById(R.id.end_recurring_date_picker))
                .setText(DATE_FORMAT.format(recurrenceCal.getTime()));
    }

    private void rebuildCourseList() {
        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        Spinner spinner = (Spinner) findViewById((R.id.new_event_parent_course_spinner));
        List<Course> coursesCopy = new ArrayList<>(courseManager.getCourses());
        ArrayAdapter<Course> courseAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, coursesCopy);
        courseAdapter.add(TEMP_COURSE);
        courseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(courseAdapter);
        spinner.setTag(TAG_COURSES);
        spinner.setOnItemSelectedListener(this);
    }

    private void regenerateNextCourseDate() {
        Spinner courseSpinner =
                (Spinner) findViewById((R.id.new_event_parent_course_spinner));
        Course newParentCourse = (Course) courseSpinner.getSelectedItem();
        ArrayList<CourseMeeting> meetings =
                newParentCourse.getCourseTimes().getSortedMeetings();
        if(!meetings.isEmpty()) {
            CourseMeeting earliestMeeting = meetings.get(0);
            Calendar now = Calendar.getInstance(TimeZone.getDefault());
            for (CourseMeeting c : meetings) {
                if(c.getCalendarDay() > now.get(Calendar.DAY_OF_WEEK) &&
                        earliestMeeting.compareTo(c) > 0) {
                    earliestMeeting = c;
                }
            }
            while(cal.get(Calendar.DAY_OF_WEEK) != earliestMeeting.getCalendarDay()) {
                cal.add(Calendar.DAY_OF_MONTH, 1);
            }
            cal.set(Calendar.HOUR_OF_DAY, earliestMeeting.getStartHour());
            cal.set(Calendar.MINUTE, earliestMeeting.getStartMinute());

        } else { cal = Calendar.getInstance(TimeZone.getDefault()); }

        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        ((TextView)findViewById(R.id.date_picker)).setText(DATE_FORMAT.format(cal.getTime()));
    }

    public void setDate(View view) {
        DatePickerDialog datePicker =
                new DatePickerDialog(this, this, this.year, this.month, this.day);
        datePicker.setCancelable(false);
        datePicker.setTitle(R.string.description_add_event_set_date);
        datePicker.getDatePicker().setTag(TAG_DUE_DATE);
        datePicker.show();
    }

    public void setRecurringEndDate(View view) {
        DatePickerDialog datePicker =
                new DatePickerDialog(this, this, this.recurYear, this.recurMonth, this.recurDay);
        datePicker.setCancelable(false);
        datePicker.setTitle(R.string.description_add_event_set_date);
        datePicker.getDatePicker().setTag(TAG_RECURRENCE);
        datePicker.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String spinnerTag = parent.getTag().toString();
        if(spinnerTag != null) {
            if(spinnerTag == TAG_COURSES) {
                if(position == parent.getCount() - 1) {
                    Intent intent = new Intent(view.getContext(), AddNewCourseActivity.class);
                    startActivityForResult(intent, CREATE_COURSE_CALLBACK_MENU);
                } else if(!dateMutated) {
                    regenerateNextCourseDate();
                }
            } else if(spinnerTag == TAG_TITLES) {
                if(position == 0) {
                    findViewById(R.id.new_event_title).setEnabled(false);
                } else {
                    findViewById(R.id.new_event_title).setEnabled(true);
                }
            } else if(spinnerTag == TAG_DUE_DATE_SPINNER) {
                View datePicker = findViewById(R.id.date_picker);
                switch(position) {
                    case 0:
                        dateMutated = false;
                        datePicker.setClickable(false);
                        datePicker.setEnabled(false);
                        regenerateNextCourseDate();
                        break;
                    case 1:
                        dateMutated = false;
                        datePicker.setClickable(false);
                        datePicker.setEnabled(false);
                        cal = Calendar.getInstance(TimeZone.getDefault());
                        cal.add(Calendar.DAY_OF_MONTH, 7);
                        break;
                    case 2:
                        dateMutated = false;
                        datePicker.setClickable(false);
                        datePicker.setEnabled(false);
                        cal = Calendar.getInstance(TimeZone.getDefault());
                        cal.add(Calendar.DAY_OF_MONTH, 14);
                        break;
                    case 3:
                        dateMutated = true;
                        datePicker.setClickable(true);
                        datePicker.setEnabled(true);
                }

                day = cal.get(Calendar.DAY_OF_MONTH);
                month = cal.get(Calendar.MONTH);
                year = cal.get(Calendar.YEAR);

                ((TextView) findViewById(R.id.date_picker))
                        .setText(DATE_FORMAT.format(cal.getTime()));
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Spinner spinner = (Spinner) findViewById(R.id.new_event_parent_course_spinner);
        switch(requestCode) {
            case CREATE_COURSE_CALLBACK_MENU:
            case CREATE_COURSE_CALLBACK_ACTION_BAR:
                switch(resultCode) {
                    case Activity.RESULT_OK:
                        rebuildCourseList();
                        spinner.setSelection(spinner.getCount() - 2);
                        break;
                    case Activity.RESULT_CANCELED:
                    default:
                        spinner.setSelection(0);
                }
                if(!dateMutated) {
                    regenerateNextCourseDate();
                }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        String dateTag = view.getTag().toString();
        if(dateTag != null) {
            if (dateTag == TAG_DUE_DATE) {
                this.day = day;
                this.month = month;
                this.year = year;
                cal.set(Calendar.DAY_OF_MONTH, day);
                cal.set(Calendar.MONTH, month);
                cal.set(Calendar.YEAR, year);
                ((TextView) findViewById(R.id.date_picker)).setText(DATE_FORMAT.format(cal.getTime()));
                dateMutated = true;
            } else if (dateTag == TAG_RECURRENCE) {
                this.recurDay = day;
                this.recurMonth = month;
                this.recurYear = year;
                recurrenceCal.set(Calendar.DAY_OF_MONTH, day);
                recurrenceCal.set(Calendar.MONTH, month);
                recurrenceCal.set(Calendar.YEAR, year);
                ((TextView) findViewById(R.id.end_recurring_date_picker)).setText(DATE_FORMAT.format(recurrenceCal.getTime()));
            }
        }
    }

    public void onSubmit(View v) {
        Spinner courseSpinner = (Spinner) findViewById(R.id.new_event_parent_course_spinner);
        Course newParentCourse = (Course) courseSpinner.getSelectedItem();

        AssignmentType assignmentType = null;
        String type =
                ((Spinner) findViewById(R.id.new_event_type_spinner)).getSelectedItem().toString();
        int assignmentCount = 0;

        if(type.equals(Homework.class.getSimpleName())) {
            assignmentType = AssignmentType.HOMEWORK;
            assignmentCount = newParentCourse.getAssignments().getHomeworks().size();
        } else if(type.equals(Quiz.class.getSimpleName())) {
            assignmentType = AssignmentType.QUIZ;
            assignmentCount = newParentCourse.getAssignments().getQuizzes().size();
        } else if(type.equals(Project.class.getSimpleName())) {
            assignmentType = AssignmentType.PROJECT;
            assignmentCount = newParentCourse.getAssignments().getProjects().size();
        } else if(type.equals(Exam.class.getSimpleName())) {
            assignmentType = AssignmentType.EXAM;
            assignmentCount = newParentCourse.getAssignments().getExams().size();
        } else if(type.equals(Final.class.getSimpleName())) {
            assignmentType = AssignmentType.FINAL;
            assignmentCount = newParentCourse.getAssignments().getFinals().size();
        }

        String title =  ((EditText) findViewById(R.id.new_event_title)).getText().toString();
        if(title == null || title.isEmpty()) {
            title = getString(R.string.assignment_name_default);
        }

        Date dueDate = cal.getTime();
        Date recurrenceEndDate = recurrenceCal.getTime();

        Spinner dueDateSpinner = (Spinner) findViewById(R.id.new_event_due_date_spinner);
        int dueDatePref = dueDateSpinner.getSelectedItemPosition();

        Spinner prioritySpinner = (Spinner) findViewById(R.id.new_event_priority_spinner);
        Priority priority = (Priority) prioritySpinner.getSelectedItem();

        Spinner recurrenceSpinner = (Spinner) findViewById(R.id.new_event_recurring_spinner);
        Recurrence recurrence = (Recurrence) recurrenceSpinner.getSelectedItem();

        String notes = ((EditText) findViewById(R.id.new_event_notes)).getText().toString();

        String gradeWeight =
                ((EditText) findViewById(R.id.new_event_grade_weight)).getText().toString();
        if(gradeWeight == null || gradeWeight.isEmpty() || gradeWeight.equals(".") || gradeWeight.equals(".0")){
            gradeWeight = "0";
        }

        String gradeValue =
                ((EditText) findViewById(R.id.new_event_grade_value)).getText().toString();
        if(gradeValue == null || gradeValue.isEmpty() || gradeWeight.equals(".") || gradeWeight.equals(".0")) {
            gradeValue = "0";
        }

        Spinner titleSpinner = (Spinner) findViewById(R.id.new_event_title_type_spinner);
        switch(titleSpinner.getSelectedItemPosition()) {
            case 0:
                title = newParentCourse.getCourseTitle() + SEPARATOR + type + SEPARATOR +
                        (assignmentCount + 1);
                break;
            case 1:
                title = newParentCourse.getCourseTitle() + SEPARATOR + type + SEPARATOR + title;
                break;
            case 2:
                title = newParentCourse.getCourseTitle() + SEPARATOR + title;
        }

        Assignment assignment =
                AssignmentFactory.createAssignment(assignmentType, title, newParentCourse,
                        dueDate, priority, recurrence, recurrenceEndDate, notes,
                        Double.parseDouble(gradeWeight), Double.parseDouble(gradeValue));

        assignment.setDueDatePref(dueDatePref);

        newParentCourse.addAssignment(assignment);
        Alarms.RemindAssignment(this, assignment);

        InternalStorage.refreshInternalMemory(getApplicationContext(), courseManager);

        Toast.makeText(getBaseContext(), R.string.add_new_event_saved, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder quitAlert = new AlertDialog.Builder(this);
        quitAlert.setTitle(getString(R.string.title_dialog_quit_without_saving));
        quitAlert.setMessage(R.string.description_dialog_quit_without_saving);

        quitAlert.setPositiveButton(getString(R.string.dialog_quit_affirmative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        quitAlert.setNegativeButton(getString(R.string.dialog_negative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { }
                });

        quitAlert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_new_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_course) {
            Intent intent = new Intent(this, AddNewCourseActivity.class);
            startActivityForResult(intent, CREATE_COURSE_CALLBACK_ACTION_BAR);
        }

        return super.onOptionsItemSelected(item);
    }
}
