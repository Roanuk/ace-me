package com.roanuk.aceme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

public class GradeListAdapter extends ArrayAdapter<Assignment> implements Serializable {
    public GradeListAdapter(Context context, ArrayList<Assignment> assignment) {
        super(context, 0, assignment);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Assignment assignment = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.assignment_grade_layout, parent, false);
        }
        // Lookup view for data population
        TextView tvCourse = (TextView) convertView.findViewById(R.id.AssignmentName);
        TextView tvType = (TextView) convertView.findViewById(R.id.GradeNumber);
        TextView tvName = (TextView) convertView.findViewById(R.id.Percent);
        // Populate the data into the template view using the data object
        tvCourse.setText(assignment.getTitle());
        tvType.setText("" + assignment.getGrade());
        tvName.setText("%");
        // Return the completed view to render on screen
        return convertView;
    }
}
