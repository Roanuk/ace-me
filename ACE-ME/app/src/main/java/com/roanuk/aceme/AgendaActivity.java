package com.roanuk.aceme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class AgendaActivity extends Activity {

    CourseManager courseManager;
    NoteManager noteManager;
    private static ArrayList<Assignment> assignments = new ArrayList<>();
    private static int day;
    private static int month;
    private static int year;
    public static int currentDayOfWeek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        noteManager = (NoteManager) InternalStorage.readNote(getApplicationContext());



        rebuild();
    }

    public void rebuild(){
        CalendarView calendar = (CalendarView) findViewById(R.id.calendarView);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(calendar.getDate());

        rebuildInfo(cal);
    }

    public void rebuildInfo(Calendar c){
        Calendar cal = c;
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DAY_OF_MONTH);
        year = cal.get(Calendar.YEAR) - 2000;
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        currentDayOfWeek = dayOfWeek;
        ((TextView) findViewById(R.id.textView29)).setText("" + (month+1) + "/" + day + "/" + year);

        CalendarView calendarView = (CalendarView) findViewById(R.id.calendarView);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();

                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                cal.set(Calendar.MONTH, month);

                AgendaActivity.setYear(year);
                AgendaActivity.setMonth(month);
                AgendaActivity.setDay(dayOfMonth);

                rebuildInfo(cal);
            }
        });

        ArrayList<Course> courses = null;
        switch(dayOfWeek){
            case 1:
                courses = getSunday();
                break;
            case 2:
                courses = getMonday();
                break;
            case 3:
                courses = getTuesday();
                break;
            case 4:
                courses = getWednesday();
                break;
            case 5:
                courses = getThursday();
                break;
            case 6:
                courses = getFriday();
                break;
            case 7:
                courses = getSaturday();
                break;
            default:
                courses = new ArrayList<>();
                break;
        }

        ArrayList<Course> courses2 = new ArrayList<>();
        for(int i = 0; i < courses.size(); i++){
            Calendar c1 = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            c1.setTime(courses.get(i).getStartDate());
            c2.setTime(courses.get(i).getEndDate());
            int smonth = c1.get(Calendar.MONTH);
            int sday = c1.get(Calendar.DAY_OF_MONTH);
            int syear = c1.get(Calendar.YEAR) - 2000;
            int emonth = c2.get(Calendar.MONTH);
            int eday = c2.get(Calendar.DAY_OF_MONTH);
            int eyear = c2.get(Calendar.YEAR) - 2000;

            if(syear > year){
            }
            else if(smonth > month && syear == year){
            }
            else if(sday > day && smonth == month && syear == year){
            }
            else if(eyear < year){
            }
            else if(emonth < month && eyear == year){
            }
            else if(eday < day && emonth == month && eyear == year){
            }
            else{
                if(courses2.contains(courses.get(i))){

                } else {
                    courses2.add(courses.get(i));
                }
            }
        }

        if(courses2.size() > 0) {
            TodayCourseAdapter adapter = new TodayCourseAdapter(this, courses2);
            ListView listView = ((ListView) findViewById(R.id.listView3));
            listView.setAdapter(adapter);
            listView.setVisibility(View.VISIBLE);
            setListViewHeightBasedOnChildren(listView);
        }
        else {
            ListView listView = ((ListView) findViewById(R.id.listView3));
            setListViewHeightBasedOnChildren(listView);
            listView.setVisibility(View.INVISIBLE);
        }

        assignments = new ArrayList<>();
        ArrayList<Assignment> assignments2 = courseManager.getUpcomingAssignments(c.getTime());
        for(Assignment a: assignments2){
            Date date = a.getDueDate();
            if(date == null){
                continue;
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int month2 = calendar.get(Calendar.MONTH);
            int day2 = calendar.get(Calendar.DAY_OF_MONTH);
            int year2 = calendar.get(Calendar.YEAR) - 2000;
            if(month == month2 && day == day2 && year == year2){
                assignments.add(a);
            }
        }

        if(assignments.size() > 0) {
            UpcomingListAdapter adapter2 = new UpcomingListAdapter(this, assignments);
            ListView listView2 = ((ListView) findViewById(R.id.listView4));
            listView2.setAdapter(adapter2);
            listView2.setVisibility(View.VISIBLE);
            setListViewHeightBasedOnChildren(listView2);
        }
        else {
            ListView listView2 = ((ListView) findViewById(R.id.listView4));
            setListViewHeightBasedOnChildren(listView2);
            listView2.setVisibility(View.INVISIBLE);
        }

        String notesText = null;
        ArrayList<AgendaNote> notes = noteManager.getNotes();
        for(AgendaNote note: notes){
            Date date = note.getDate();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int month2 = calendar.get(Calendar.MONTH);
            int day2 = calendar.get(Calendar.DAY_OF_MONTH);
            int year2 = calendar.get(Calendar.YEAR) - 2000;
            if(month == month2 && day == day2 && year == year2){
                notesText = note.getNotes();
            }
        }

        ((TextView) findViewById(R.id.toDo)).setText(notesText);
    }

    private static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        if(listAdapter.getCount() > 0) {

            int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                if (listItem instanceof ViewGroup) {
                    listItem.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                }
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
        } else {

        }
    }

    public ArrayList<Course> getSunday(){
        ArrayList<Course> courses = courseManager.getCourses();
        ArrayList<Course> sunday = new ArrayList<>();
        for(Course course: courses){
            ArrayList<CourseMeeting> meetings = course.getCourseTimes().getMeetings();
            for(CourseMeeting meeting: meetings){
                Day day = meeting.getDay();
                if(day.equals(Day.SUNDAY) || day == Day.SUNDAY){
                    sunday.add(course);
                }
            }
        }
        return sunday;
    }

    public ArrayList<Course> getMonday(){
        ArrayList<Course> courses = courseManager.getCourses();
        ArrayList<Course> monday = new ArrayList<>();
        for(Course course: courses){
            ArrayList<CourseMeeting> meetings = course.getCourseTimes().getMeetings();
            for(CourseMeeting meeting: meetings){
                Day day = meeting.getDay();
                if(day.equals(Day.MONDAY) || day == Day.MONDAY){
                    monday.add(course);
                }
            }
        }
        return monday;
    }

    public ArrayList<Course> getTuesday(){
        ArrayList<Course> courses = courseManager.getCourses();
        ArrayList<Course> monday = new ArrayList<>();
        for(Course course: courses){
            ArrayList<CourseMeeting> meetings = course.getCourseTimes().getMeetings();
            for(CourseMeeting meeting: meetings){
                Day day = meeting.getDay();
                if(day.equals(Day.TUESDAY) || day == Day.TUESDAY){
                    monday.add(course);
                }
            }
        }
        return monday;
    }

    public ArrayList<Course> getWednesday(){
        ArrayList<Course> courses = courseManager.getCourses();
        ArrayList<Course> monday = new ArrayList<>();
        for(Course course: courses){
            ArrayList<CourseMeeting> meetings = course.getCourseTimes().getMeetings();
            for(CourseMeeting meeting: meetings){
                Day day = meeting.getDay();
                if(day.equals(Day.WEDNESDAY) || day == Day.WEDNESDAY){
                    monday.add(course);
                }
            }
        }
        return monday;
    }

    public ArrayList<Course> getThursday(){
        ArrayList<Course> courses = courseManager.getCourses();
        ArrayList<Course> monday = new ArrayList<>();
        for(Course course: courses){
            ArrayList<CourseMeeting> meetings = course.getCourseTimes().getMeetings();
            for(CourseMeeting meeting: meetings){
                Day day = meeting.getDay();
                if(day.equals(Day.THURSDAY) || day == Day.THURSDAY){
                    monday.add(course);
                }
            }
        }
        return monday;
    }

    public ArrayList<Course> getFriday(){
        ArrayList<Course> courses = courseManager.getCourses();
        ArrayList<Course> monday = new ArrayList<>();
        for(Course course: courses){
            ArrayList<CourseMeeting> meetings = course.getCourseTimes().getMeetings();
            for(CourseMeeting meeting: meetings){
                Day day = meeting.getDay();
                if(day.equals(Day.FRIDAY) || day == Day.FRIDAY){
                    monday.add(course);
                }
            }
        }
        return monday;
    }

    public ArrayList<Course> getSaturday(){
        ArrayList<Course> courses = courseManager.getCourses();
        ArrayList<Course> monday = new ArrayList<>();
        for(Course course: courses){
            ArrayList<CourseMeeting> meetings = course.getCourseTimes().getMeetings();
            for(CourseMeeting meeting: meetings){
                Day day = meeting.getDay();
                if(day.equals(Day.SATURDAY) || day == Day.SATURDAY){
                    monday.add(course);
                }
            }
        }
        return monday;
    }

    public void saveToDo(View view){
        String notes = ((TextView) findViewById(R.id.toDo)).getText().toString();
        CalendarView calendar = (CalendarView) findViewById(R.id.calendarView);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(calendar.getDate());
        Date date = cal.getTime();

        noteManager.addNote(new AgendaNote(notes, date));

        InternalStorage.refreshInternalNote(getApplicationContext(), noteManager);

        Toast.makeText(getApplicationContext(), "To Do Saved!", Toast.LENGTH_LONG).show();
    }

    public static void setDay(int day) {
        AgendaActivity.day = day;
    }

    public static void setMonth(int month) {
        AgendaActivity.month = month;
    }

    public static void setYear(int year) {
        AgendaActivity.year = year;
    }

    public void settingsAgendaClickEvent(View view)
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void newEventAgendaClickEvent(View view)
    {
        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        if(courseManager.getCourses().isEmpty()) {
            Intent intent = new Intent(this, AddNewCourseActivity.class);
            startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(this, AddNewEventActivity.class);
            startActivity(intent);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Calendar cal = Calendar.getInstance();
        CalendarView view = (CalendarView) findViewById(R.id.calendarView);
        cal.setTimeInMillis(view.getDate());

        AgendaActivity.setYear(cal.get(Calendar.YEAR));
        AgendaActivity.setMonth(cal.get(Calendar.MONTH));
        AgendaActivity.setDay(cal.get(Calendar.DAY_OF_MONTH));

        courseManager = (CourseManager) InternalStorage.readObject(getApplicationContext());
        rebuildInfo(cal);
    }

    public static int getCurrentDayOfWeek() {
        return currentDayOfWeek;
    }
}
