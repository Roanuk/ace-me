package com.roanuk.aceme;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Class used to hold the start and end times of course meetings such as lecture and labs
 */
public class CourseTimes implements Serializable
{
    ArrayList<CourseMeeting> meetings;

    public CourseTimes()
    {
        meetings = new ArrayList<>();
    }

    public void addTime(Day day, int startHour, int startMinute, int endHour, int endMinute, String room){
        CourseMeeting cm = new CourseMeeting(day, startHour, startMinute, endHour, endMinute, room);
        meetings.add(cm);
    }

    public void removeTime(CourseMeeting cm){
        if(meetings.contains(cm))
            meetings.remove(cm);
    }

    public ArrayList<CourseMeeting> getMeetings(){
        return meetings;
    }

    public ArrayList<CourseMeeting> getSortedMeetings() {
        Collections.sort(meetings);
        return meetings;
    }
}
