package com.roanuk.aceme;

import java.util.Date;

/**
 * Created by Bryson on 4/2/2015.
 */
public class CourseFactory {

    /**
     * method used to create a course with specified parameters
     * @param title title of the course
     * @param courseCode course code of the course
     * @param start start date of the course
     * @param end end date of the course
     * @param times times of course meetings
     * @param notes user's notes for the course
     * @param prof the professor
     * @param priority the priority of the course
     * @param syllabus the path of the syllabus
     * @param gradeSystem the grade system of the course
     * @return the created course
     */
    public static Course createCourse(String title, String courseCode, Date start, Date end,
                                      CourseTimes times, String notes, String prof,
                                      Priority priority, String syllabus, GradeSystem gradeSystem){
        Course course = new Course();
        course.setCourseTitle(title);
        course.setCourseCode(courseCode);
        course.setStartDate(start);
        course.setEndDate(end);
        course.setCourseTimes(times);
        course.setProfessor(prof);
        course.setPriority(priority);
        course.setSyllabusPath(syllabus);
        course.setNotes(notes);
        course.setGradeSystem(gradeSystem);
        return course;
    }

    /**
     * method used to create an empty course
     * @return the created course
     */
    public static Course createCourse(){
        return new Course();
    }
}
